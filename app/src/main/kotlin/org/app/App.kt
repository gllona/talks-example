package org.app

import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.runBlocking
import org.apache.logging.log4j.LogManager
import org.logicas.librerias.talks.api.AppApi
import org.logicas.librerias.talks.api.ModelApi
import org.logicas.librerias.talks.api.TalkType
import org.logicas.librerias.talks.api.TalksConfiguration
import org.logicas.librerias.talks.config.LibConfig

@Suppress("UNUSED_PARAMETER")
class App private constructor(args: Array<String>) : AppApi {

    companion object {
        private val logger = LogManager.getLogger(App::class.java)

        @JvmStatic
        fun main(args: Array<String>) {
            App(args)
        }
    }

    private var talksConfig: TalksConfiguration
    private var model: ModelApi

    init {
        runBlocking {
            logger.info("Starting app....")
            AppConfig.getInstance().init()
            talksConfig = buildTalksConfiguration()
            model = startModel()
            setConfigInterceptors()
            talksConfig.inputAdaptersManager.startInputAdapters()
            registerShutDownHook()
            logger.info("App started.")
        }
    }

    private fun buildTalksConfiguration(): TalksConfiguration {
        return object : TalksConfiguration() {
            override fun getApp(): AppApi {
                return this@App
            }

            override fun getModel(): ModelApi {
                return Model.getInstance(this)
            }

            override fun getTalkTypes(): List<TalkType> {
                return Talks.getAllTalkTypes()
            }
        }
    }

    private fun startModel(): ModelApi {
        logger.info("Starting Model....")
        val model = talksConfig.model
        logger.info("Model started.")
        return model
    }

    private fun setConfigInterceptors() {
        val configInterceptor = ConfigInterceptor.getInstance(talksConfig)
        LibConfig.getInstance().setInterceptor(configInterceptor)
        AppConfig.getInstance().setInterceptor(configInterceptor)
    }

    private fun registerShutDownHook() {
        Runtime.getRuntime().addShutdownHook(
            Thread {
                runBlocking {
                    shutdown()
                }
            }
        )
    }

    override suspend fun shutdown() = coroutineScope {
        logger.info("Stopping app....")
        talksConfig.inputAdaptersManager.stopInputAdapters()
        model.shutdown()
        logger.info("App stopped.")
    }
}
