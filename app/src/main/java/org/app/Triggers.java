package org.app;

public class Triggers {

    public static final String NOT_A_COMMAND = "[^/][\\s\\S]*";
    public static final String FOUR_DIGITS = "[0-9]{4}";
    public static final String POSITIVE_INTEGER = "[0-9]{1,8}";
}
