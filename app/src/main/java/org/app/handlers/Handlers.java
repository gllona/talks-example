package org.app.handlers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.logicas.librerias.talks.api.TalkType;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.engine.*;

import java.util.Optional;

public class Handlers extends Talk {

    private static final Logger logger = LogManager.getLogger(Handlers.class);

    public Handlers(TalksConfiguration talksConfig, TalkType talkType) {
        super(talksConfig, talkType);
        createContext("Main");
    }

    @Override
    protected Optional<InputHandler> getDefaultHandler() {
        return Optional.of(InputHandler.of("handlers.Handlers::main",
            InputTrigger.ofString("/handlers")));
    }

    public TalkResponse main(TalkRequest call) {
        return TalkResponse.ofText(
            call.getChatId(),
            "Test InputHandlers" +
                "\n" +
                "\n/test_1 (PRESERVE_HANDLERS)" +
                "\nGo back to /intro"
        ).withString("handlers.Handlers::test1", "/test_1")
         .withString("main.Main::menu", "/intro");
    }

    public TalkResponse test1(TalkRequest call) {
        return TalkResponse.ofText(
            call.getChatId(),
            "1. Will send one message with 1 button as handler" +
                "\n2. Will wait for 30 secs (so you can query the DB)" +
                "\n3. Will send one message with PRESERVE_HANDLERS" +
                "\nIt's expected that you can use handler/button from (1)"
        ).withButton("handlers.Handlers::test1Start", "Start");
    }

    public TalkResponse test1Start(TalkRequest call) {
        call.getInputAdapter().sendResponse(
            TalkResponse.ofText(
                call.getChatId(),
                "First message with button"
            ).withButton("handlers.Handlers::buttonClicked", "Button")
        );

        try {
            Thread.sleep(30 * 1000);
        } catch (InterruptedException ignored) {
        }

        return TalkResponse.ofText(
            call.getChatId(),
            "Second message with PRESERVE_HANDLERS"
        ).withPreserveHandlers();
    }

    public TalkResponse buttonClicked(TalkRequest call) {
        return TalkResponse.ofText(
            call.getChatId(),
            "You clicked on " + call.getMessage().text() + "!"
        ).withPreserveHandlers();
    }
}
