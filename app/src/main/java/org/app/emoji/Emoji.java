package org.app.emoji;

import com.vdurmont.emoji.EmojiParser;
import java.util.Optional;
import org.logicas.librerias.talks.api.TalkType;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.engine.InputHandler;
import org.logicas.librerias.talks.engine.InputTrigger;
import org.logicas.librerias.talks.engine.Talk;
import org.logicas.librerias.talks.engine.TalkRequest;
import org.logicas.librerias.talks.engine.TalkResponse;

public class Emoji extends Talk {

    public Emoji(TalksConfiguration talksConfig, TalkType talkType) {
        super(talksConfig, talkType);
        createContext("Main");
    }

    @Override
    public Optional<InputHandler> getDefaultHandler() {
        return Optional.of(
            InputHandler.of("emoji.Emoji::parse",
                InputTrigger.ofString("/emoji"))
        );
    }

    public TalkResponse parse(TalkRequest request) {
        String lastEmojiText = getContext().getString(request.getChatId(), "LAST_EMOJI_TEXT");
        if (lastEmojiText != null) {
            request.getInputAdapter().sendResponse(
                TalkResponse.ofText(
                    request.getChatId(),
                    "Last Emoji text sent:"
                        + "\n"
                        + "\n" + lastEmojiText
                )
            );
        }

        return TalkResponse.ofText(
            request.getChatId(),
            "Send emoji 👋"
        ).withButton("emoji.Emoji::emojiButton", "Emoji 🕹 Button")
         .withRegex("emoji.Emoji::doParse", ".*");
    }

    public TalkResponse emojiButton(TalkRequest request) {
        return TalkResponse.ofText(
            request.getChatId(),
            "You pressed on:"
                + "\n"
                + "\n" + request.getMessage().text()
        ).withPreserveHandlers();
    }

    public TalkResponse doParse(TalkRequest request) {
        String textWithEmoji = request.getMessage().text();

        getContext().set(
            request.getChatId(),
            "LAST_EMOJI_TEXT",
            textWithEmoji
        );

        String parsedText = EmojiParser.parseToHtmlHexadecimal(textWithEmoji);

        // two different outputs according to ENCODE_EMOJI setting
        return TalkResponse.ofText(
            request.getChatId(),
            parsedText
        ).withPreserveHandlers();
    }
}
