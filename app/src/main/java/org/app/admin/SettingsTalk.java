package org.app.admin;

import org.apache.commons.lang3.tuple.Pair;
import org.app.ConfigInterceptor;
import org.logicas.librerias.talks.api.TalkType;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.engine.InputHandler;
import org.logicas.librerias.talks.engine.Talk;
import org.logicas.librerias.talks.engine.TalkRequest;
import org.logicas.librerias.talks.engine.TalkResponse;

import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Collectors;

public class SettingsTalk extends Talk {

    private AuthService authService = AuthService.getInstance(talksConfig);
    private ConfigInterceptor configInterceptor = ConfigInterceptor.getInstance(talksConfig);

    public SettingsTalk(TalksConfiguration talksConfig, TalkType talkType) {
        super(talksConfig, talkType);
        createContext("Main");
    }

    @Override
    public Optional<InputHandler> getDefaultHandler() {
        return Optional.empty();
    }

    public TalkResponse settings(TalkRequest request) {
        if (! authService.userCanModifySettings(request.getChatId())) {
            return unauthorizedResponse(request);
        }

        TalkResponse talkResponse = TalkResponse.ofText(
            request.getChatId(),
            "Current settings:\n" +
                "\n" +
                getSettingValues() + "\n" +
                "\n" +
                "Choose setting to toggle or set value:"
        );

        decorateWithSettings(talkResponse);

        return talkResponse;
    }

    private String getSettingValues() {
        return Arrays.stream(Setting.values())
            .map(setting -> Pair.of(setting, getSettingValue(setting)))
            .filter(pair -> pair.getRight() != null)
            .map(pair -> pair.getLeft().name() + " = " + pair.getRight())
            .collect(Collectors.joining("\n"));
    }

    private String getSettingValue(Setting setting) {
        return setting.getValueSupplier().apply(setting);
    }

    private void decorateWithSettings(TalkResponse talkResponse) {
        for (Setting setting: Setting.values()) {
            talkResponse.withButton("admin.SettingsTalk::setSetting", setting.name());
        }
        talkResponse.withButton("admin.AdminTalk::admin", "Back");
    }

    public TalkResponse setSetting(TalkRequest request) {
        String settingName = request.getMessage().text();
        Setting setting = Setting.from(settingName);

        String message = setting.getAction().apply(configInterceptor, setting);

        return TalkResponse.ofText(
            request.getChatId(),
            message
        ).withButton("admin.SettingsTalk::settings", "Continue");
    }

    private TalkResponse unauthorizedResponse(TalkRequest request) {
        return TalkResponse.ofText(
            request.getChatId(),
            "Not authorized."
        ).withPreserveHandlers();
    }
}
