package org.app.admin;

import java.util.List;

import static java.util.Arrays.asList;

public enum Role {

    ADMIN,
    OPERATOR,
    USER;

    public static Role from(String text) {
        if (text == null) {
            return null;
        }
        try {
            return Role.valueOf(text);
        } catch (Exception e) {
            return null;
        }
    }

    public static List<Role> privileged() {
        return asList(ADMIN, OPERATOR);
    }
}
