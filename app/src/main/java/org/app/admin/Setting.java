package org.app.admin;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.app.ConfigInterceptor;
import org.logicas.librerias.talks.config.LibSetting;

import java.util.Arrays;
import java.util.function.BiFunction;
import java.util.function.Function;

@AllArgsConstructor
@Getter
public enum Setting {

    EMERGENCY_MODE(
        false,
        setting -> setting.enabled.toString(),
        (configInterceptor, setting) -> {
            setting.enabled = ! setting.enabled;
            if (setting.enabled) {
                Integer newValue = 48 * 60; // all flying messages in the next 48 hours will fly in a minute - per schedule, but max a message flies per second
                configInterceptor.setPropertyValue(
                    LibSetting.FLYING_MESSAGES_STEALTH_FACTOR.name(),
                    newValue.toString()
                );
                return "Emergency mode ON!!!";
            } else {
                configInterceptor.dropPropertyValue(LibSetting.FLYING_MESSAGES_STEALTH_FACTOR.name());
                return "Emergency mode off.";
            }
        }
    );

    private Boolean enabled;
    private final Function<Setting, String> valueSupplier;
    @Getter private final BiFunction<ConfigInterceptor, Setting, String> action;

    public static Setting from(String name) {
        return Arrays.stream(values())
            .filter(setting -> setting.name().equals(name))
            .findFirst()
            .orElseThrow(() -> new RuntimeException("Setting with name " + name + " not found"));
    }
}
