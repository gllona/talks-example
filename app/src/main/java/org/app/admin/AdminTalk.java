package org.app.admin;

import org.logicas.librerias.talks.api.TalkType;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.engine.*;

import java.util.Optional;

public class AdminTalk extends Talk {

    private AuthService authService = AuthService.getInstance(talksConfig);

    public AdminTalk(TalksConfiguration talksConfig, TalkType talkType) {
        super(talksConfig, talkType);
        createContext("Main");
    }

    @Override
    public Optional<InputHandler> getDefaultHandler() {
        return Optional.of(InputHandler.of("admin.AdminTalk::admin", InputTrigger.ofString("/admin")));
    }

    public TalkResponse admin(TalkRequest request) {
        if (! authService.userCanAuthorize(request.getChatId())) {
            return unauthorizedResponse(request);
        }

        return TalkResponse.ofText(
            request.getChatId(),
            "Select one or go back to /start"
           ).withButton("admin.AuthTalk::authorizations", "Authorizations")
            .withButton("admin.SettingsTalk::settings", "Settings");
    }

    private TalkResponse unauthorizedResponse(TalkRequest request) {
        return TalkResponse.ofText(
            request.getChatId(),
            "Not authorized."
           ).withPreserveHandlers();
    }
}
