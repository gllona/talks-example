package org.app;

import com.pengrad.telegrambot.model.Update;
import org.logicas.librerias.talks.api.InputAdapterApi;
import org.logicas.librerias.talks.api.ModelApi;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.engine.TalkRequest;
import org.logicas.librerias.talks.engine.TalkResponse;

import java.util.Optional;

public class Model implements ModelApi {

    private static ModelApi instance;

    private TalksConfiguration talksConfig;

    public static synchronized ModelApi getInstance(TalksConfiguration talksConfig) {
        if (instance == null) {
            instance = new Model(talksConfig);
        }
        return instance;
    }

    private Model(TalksConfiguration talksConfig) {
        this.talksConfig = talksConfig;
        talksConfig.getTalkManager();
    }

    @Override
    public void shutdown() {
        talksConfig.getTalkManager().shutDown(); // this will shut down the DB connector also
    }

    @Override
    public Optional<TalkResponse> receiveMessage(InputAdapterApi inputAdapter, Update update) {
        return talksConfig.getTalkManager().dispatch(new TalkRequest(
            inputAdapter,
            update
        ));
    }

    // OTHER API CALLS GO HERE (e.g. REST CALLS)
}
