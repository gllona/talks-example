package org.app;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.app.admin.AdminTalk;
import org.app.admin.AuthTalk;
import org.app.admin.SettingsTalk;
import org.app.buttons.Buttons;
import org.app.communicator.CommunicatorTalk;
import org.app.communicator.ContactTalk;
import org.app.eatings.Eatings;
import org.app.eatings.EatingsToo;
import org.app.emoji.Emoji;
import org.app.flyingmessages.FlyingMessages;
import org.app.groups.JoinGroup;
import org.app.groups.LeaveGroup;
import org.app.handlers.Handlers;
import org.app.localisation.DrosophilaMelanogaster;
import org.app.main.Fallback;
import org.app.main.Main;
import org.app.media.Media;
import org.app.priorities.Priorities;
import org.app.xarxa.*;
import org.logicas.librerias.talks.api.TalkType;
import org.logicas.librerias.talks.engine.Talk;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@Getter
public enum Talks implements TalkType {

    // Important: All enum elements should have the same name and capitalization as the class name
    Eatings(Eatings.class),
    EatingsToo(EatingsToo.class),
    Main(Main.class),
    Xarxa(Xarxa.class),
    XarxaPersonas(XarxaPersonas.class),
    XarxaOfrecimientos(XarxaOfrecimientos.class),
    XarxaPeticiones(XarxaPeticiones.class),
    XarxaSoportes(XarxaSoportes.class),
    Media(Media.class),
    Buttons(Buttons.class),
    Handlers(Handlers.class),
    Priorities(Priorities.class),
    Emoji(Emoji.class),
    DrosophilaMelanogaster(DrosophilaMelanogaster.class),
    FlyingMessages(FlyingMessages.class),
    JoinGroup(JoinGroup.class),
    LeaveGroup(LeaveGroup.class),
    AdminTalk(AdminTalk.class),
    AuthTalk(AuthTalk.class),
    SettingsTalk(SettingsTalk.class),
    CommunicatorTalk(CommunicatorTalk.class),
    ContactTalk(ContactTalk.class),
    Fallback(Fallback.class);

    private Class<? extends Talk> talkClass;

    public static List<TalkType> getAllTalkTypes() {
        return Arrays.stream(values())
            .map(t -> (TalkType) t)
            .collect(Collectors.toList());
    }

    @Override
    public Class<? extends Talk> getTalkClass() {
        return talkClass;
    }

    @Override
    public String getName() {
        return talkClass.getSimpleName();
    }
}
