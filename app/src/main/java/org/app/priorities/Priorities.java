package org.app.priorities;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.logicas.librerias.talks.api.InputAdapterApi;
import org.logicas.librerias.talks.api.TalkType;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.engine.*;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;

public class Priorities extends Talk {

    private static final Logger logger = LogManager.getLogger(Priorities.class);

    private final AtomicLong secondChatId = new AtomicLong(-1);

    public Priorities(TalksConfiguration talksConfig, TalkType talkType) {
        super(talksConfig, talkType);
        createContext("Main");
    }

    @Override
    protected Optional<InputHandler> getDefaultHandler() {
        return Optional.of(InputHandler.of("priorities.Priorities::main",
            InputTrigger.ofString("/priorities")));
    }

    public TalkResponse main(TalkRequest call) {
        return TalkResponse.ofText(
            call.getChatId(),
            "Test low priority responses (aka notifications)" +
                "\n" +
                "\n/test_0 (no notifications)" +
                "\n/test_1 (single chat)" +
                "\n/test_2 (two chats)" +
                "\n/test_3 (hundreds of notifications)" +
                "\n/test_4 (single chat the correct form)" +
                "\nGo back to /intro"
        ).withString("priorities.Priorities::test0", "/test_0")
         .withString("priorities.Priorities::test1", "/test_1")
         .withString("priorities.Priorities::test2", "/test_2")
         .withString("priorities.Priorities::test3", "/test_3")
         .withString("priorities.Priorities::test4", "/test_4")
         .withString("main.Main::menu", "/intro");
    }

    public TalkResponse test0(TalkRequest call) {
        return TalkResponse.ofText(
            call.getChatId(),
            "1. Will send messages 1-4 with no buttons" +
                "\n2. Will send messages 5-8 with no buttons"
        ).withButton("priorities.Priorities::test0Start", "Start");
    }

    public TalkResponse test0Start(TalkRequest call) {
        sendNormalPriorityMessages(call.getInputAdapter(), call.getChatId(), 1, 4, "First message group", false);
        sendNormalPriorityMessages(call.getInputAdapter(), call.getChatId(), 5, 8, "Second messages group", false);

        return TalkResponse.ofText(
            call.getChatId(),
            "Done."
        ).withButton("priorities.Priorities::main", "Continue");
    }

    public TalkResponse test1(TalkRequest call) {
        return TalkResponse.ofText(
            call.getChatId(),
            "1. Will send messages 1-4" +
                "\n2. Will send notifications 1-4" +
                "\n3. Will send messages 5-8"
        ).withButton("priorities.Priorities::test1Start", "Start");
    }

    public TalkResponse test1Start(TalkRequest call) {
        sendNormalPriorityMessages(call.getInputAdapter(), call.getChatId(), 1, 4, "First message group", true);
        sendLowPriorityMessages(call.getInputAdapter(), call.getChatId(), 1, 4, "Notification group");
        sendNormalPriorityMessages(call.getInputAdapter(), call.getChatId(), 5, 8, "Second messages group", true);

        return TalkResponse.ofText(
            call.getChatId(),
            "Done."
        ).withButton("priorities.Priorities::main", "Continue");
    }

    public TalkResponse test2(TalkRequest call) {
        return TalkResponse.ofText(
            call.getChatId(),
            "1. On chat session 2 tap on /register_chat_id" +
                "\n2. On chat 1 press Start button" +
                "\n3. On chat 1 & 2 will send messages 1-4" +
                "\n4. On chat 1 & 2 will send notifications 1-4" +
                "\n5. On chat 1 & 2 will send messages 5-8"
        ).withString("priorities.Priorities::test2RegisterChatId", "/register_chat_id")
         .withButton("priorities.Priorities::test2Start", "Start");
    }

    public TalkResponse test2RegisterChatId(TalkRequest call) {
        secondChatId.set(call.getChatId());

        return TalkResponse.ofText(
            call.getChatId(),
            "Chat ID saved."
        ).withButton("priorities.Priorities::main", "Continue");
    }

    public TalkResponse test2Start(TalkRequest call) {
        sendNormalPriorityMessages(call.getInputAdapter(), call.getChatId(), 1, 4, "First message group to chat 1", true);
        sendNormalPriorityMessages(call.getInputAdapter(), secondChatId.get(), 1, 4, "First message group to chat 2", true);
        sendLowPriorityMessages(call.getInputAdapter(), call.getChatId(), 1, 4, "Notification group to chat 1");
        sendLowPriorityMessages(call.getInputAdapter(), secondChatId.get(), 1, 4, "Notification group to chat 2");
        sendNormalPriorityMessages(call.getInputAdapter(), call.getChatId(), 5, 8, "Second messages group to chat 1", true);
        sendNormalPriorityMessages(call.getInputAdapter(), secondChatId.get(), 5, 8, "Second messages group to chat 2", true);

        return TalkResponse.ofText(
            call.getChatId(),
            "Done."
        ).withButton("priorities.Priorities::main", "Continue");
    }

    public TalkResponse test3(TalkRequest call) {
        return TalkResponse.ofText(
            call.getChatId(),
            "1. Will send one message to current chat" +
                "\n2. Will send 300 notifications to invalid chat ID's (5 minutes delivery)" +
                "\n3. Will send one message to current chat"
        ).withButton("priorities.Priorities::test3Start", "Start");
    }

    public TalkResponse test3Start(TalkRequest call) {
        sendNormalPriorityMessages(call.getInputAdapter(), call.getChatId(), 1, 1, "First message", true);
        logger.info("TO START SENDING 300 NOTIFICATIONS");
        for (int i = 1; i <= 300; i++) {
            int chatId = -i;
            sendLowPriorityMessages(call.getInputAdapter(), chatId, i, i, "Notification to invalid chat");
        }
        logger.info("FINISHED SENDING 300 NOTIFICATIONS");
        sendNormalPriorityMessages(call.getInputAdapter(), call.getChatId(), 2, 2, "Second message", true);

        return TalkResponse.ofText(
            call.getChatId(),
            "Done."
        ).withButton("priorities.Priorities::main", "Continue");
    }

    public TalkResponse test4(TalkRequest call) {
        return TalkResponse.ofText(
            call.getChatId(),
            "1. Will send messages 1-4 with no buttons" +
                "\n2. Will send notifications 1-4" +
                "\n3. Will send messages 5-8 with no buttons"
        ).withButton("priorities.Priorities::test4Start", "Start");
    }

    public TalkResponse test4Start(TalkRequest call) {
        sendNormalPriorityMessages(call.getInputAdapter(), call.getChatId(), 1, 4, "First message group", false);
        sendLowPriorityMessages(call.getInputAdapter(), call.getChatId(), 1, 4, "Notification group");
        sendNormalPriorityMessages(call.getInputAdapter(), call.getChatId(), 5, 8, "Second messages group", false);

        return TalkResponse.ofText(
            call.getChatId(),
            "Done."
        ).withButton("priorities.Priorities::main", "Continue");
    }

    private void sendNormalPriorityMessages(InputAdapterApi inputAdapter, long chatId, int from, int to, String messagePrefix, boolean showButton) {
        for (int i = from; i <= to; i++) {
            TalkResponse response = TalkResponse.ofText(
                chatId,
                messagePrefix + " [" + i + "]"
            );
            if (showButton) {
                response.withButton("priorities.Priorities::buttonClicked", "Button " + i);
            } else {
                response.withEmptyHandlers();
            }
            inputAdapter.sendResponse(
                response
            );
        }
    }

    private void sendLowPriorityMessages(InputAdapterApi inputAdapter, long chatId, int from, int to, String messagePrefix) {
        for (int i = from; i <= to; i++) {
            inputAdapter.sendResponse(
                TalkResponse.ofText(
                    chatId,
                    messagePrefix + " [" + i + "]"
                ).withPreserveHandlers()
                 .withLowPriority()
            );
        }
    }

    public TalkResponse buttonClicked(TalkRequest call) {
        return TalkResponse.ofText(
            call.getChatId(),
            "You clicked on " + call.getMessage().text() + "!"
        ).withPreserveHandlers();
    }
}
