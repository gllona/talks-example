package org.app.main;

import java.util.Optional;
import org.logicas.librerias.talks.api.TalkType;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.engine.InputHandler;
import org.logicas.librerias.talks.engine.InputTrigger;
import org.logicas.librerias.talks.engine.Talk;
import org.logicas.librerias.talks.engine.TalkRequest;
import org.logicas.librerias.talks.engine.TalkResponse;

public class Fallback extends Talk {

    public Fallback(TalksConfiguration talksConfig, TalkType talkType) {
        super(talksConfig, talkType);
        createContext();
    }

    @Override
    protected Optional<InputHandler> getDefaultHandler() {
        return Optional.of(InputHandler.of("main.Fallback::fallback", InputTrigger.ofRegex(".*")));
    }

    public TalkResponse fallback(TalkRequest call) {
        return TalkResponse.ofText(
            call.getChatId(),
            "No puedo entender la entrada."
        ).withPreserveHandlers();
    }
}
