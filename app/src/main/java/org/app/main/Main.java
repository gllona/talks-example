package org.app.main;

import org.app.Strings;
import org.app.Strings.Lang;
import org.app.Strings.Str;
import org.app.admin.AuthService;
import org.logicas.librerias.talks.api.TalkType;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.engine.*;

import java.util.List;
import java.util.Optional;

public class Main extends Talk {

    private AuthService authService = AuthService.getInstance(talksConfig);
    private Strings strings = Strings.getInstance(this::getBotLang);

    public Main(TalksConfiguration talksConfig, TalkType talkType) {
        super(talksConfig, talkType);
        createContext();
    }

    @Override
    protected Optional<InputHandler> getDefaultHandler() {
        return Optional.of(InputHandler.of("main.Main::main", InputTrigger.ofRegex("/start( .+)?")));
    }

    public Lang getBotLang(long session) {
        String contextLang = getContext().getString(session, "LANG");
        return contextLang == null ?
            Lang.ES :
            Lang.valueOf(contextLang);
    }

    public TalkResponse main(TalkRequest call) {
        Optional<TalkResponse> initialization = authService.firstTimeRoute(call.getChatId());
        if (initialization.isPresent()) {
            return initialization.get();
        }

        authService.fillInUsers(call);

        List<String> matches = call.getMatch().getRegexMatches();
        String arguments = matches.size() == 1 ? null : matches.get(1);

        if (arguments != null) {
            return startWithArguments(call, arguments);
        }

        resetContext(call);

        return hello(call);
    }

    public TalkResponse hello(TalkRequest call) {
        if (call.getMatch().getRegexMatches().get(0).equalsIgnoreCase("/start")) {
            talksConfig.getTalkManager().resetTalkContexts(call.getChatId());   // /start should reset the bot for the user

            return TalkResponse.ofText(
                call.getChatId(),
                "Hola mundo!" +
                    "\nTu nombre es: " + call.getUpdate().message().from().firstName() +
                    "\nTu ID de Telegram es: " + call.getUpdate().message().from().id() +
                    "\nApp de Xarxa: /menu (demo - no usar para contactos reales)" +
                    "\nArea de pruebas de la plataforma: /intro"
            )
           .withString("main.Main::menu", "/intro");
        } else {
            return TalkResponse.ofText(
                call.getChatId(),
                "No puedo entender la entrada: " + call.getUpdate().message().text()
            )
            .withPreserveHandlers();
        }
    }

    public TalkResponse menu(TalkRequest call) {
        return TalkResponse.ofText(
            call.getChatId(),
            "This is the Menu" +
                "\n/helloworld" +
                "\n/purpose" +
                "\n/settings (regex)" +
                "\n/eatings (database)" +
                "\n/media" +
                "\n/buttons" +
                "\n/handlers" +
                "\n/priorities" +
                "\n/flyingmessages" +
                "\n/emoji" +
                "\n/fly (localisation)" +
                "\n/contact (anonymous users-operators communication)" +
                "\n/admin (restricted operations)" +
                "\n/start (reset bot)"
        )
        .withString("main.Main::helloworld", "/helloworld")
        .withString("main.Main::purpose", "/purpose")
        .withString("main.Main::settings", "/settings");
    }

    public TalkResponse helloworld(TalkRequest call) {
        // this method for localising strings is deprecated. For an updated method see package org.app.localisation
        return TalkResponse.ofText(
            call.getChatId(),
            strings.get(Str.HelloWorld).forSession(call.getChatId()) +
                "\n" +
                strings.get(Str.YourNameIs).with("userName", call.getUpdate().message().from().firstName()).forSession(call.getChatId()) +
                "\n/helloworld"
        )
        .withString("main.Main::helloworld", "/helloworld");
    }

    public TalkResponse purpose(TalkRequest call) {
        return TalkResponse.ofText(
            call.getChatId(),
            "The purpose is to act now"
        );
    }

    public TalkResponse settings(TalkRequest call) {
        call.getInputAdapter().sendResponse(TalkResponse.ofText(call.getChatId(), "Message 1"));
        call.getInputAdapter().sendResponse(TalkResponse.ofText(call.getChatId(), "Message 2"));
        call.getInputAdapter().sendResponse(TalkResponse.ofText(call.getChatId(), "Message 3"));
        call.getInputAdapter().sendResponse(TalkResponse.ofText(call.getChatId(), "Message 4"));

        return TalkResponse.ofText(
            call.getChatId(),
            "Settings are not implemented yet but you can test:" +
                "\n/setting_666"
        )
        .withRegex("main.Main::settingN", "/setting_([0-9]+)");
    }

    public TalkResponse settingN(TalkRequest call) {
        return TalkResponse.ofText(
            call.getChatId(),
            "This is setting #" + call.getMatch().getRegexMatches().get(1)
        );
    }

    public TalkResponse method(TalkRequest call) {
        return TalkResponse.ofText(
            call.getChatId(),
            "HEADER" +
                "\n/COMMAND"
        )
        .withString("main.Main::method", "/COMMAND");   // change main.Main::method
    }

    private TalkResponse startWithArguments(TalkRequest call, String arguments) {
        return TalkResponse.ofText(
            call.getChatId(),
            "/start with arguments is not supported yet"
        );
    }

    private void resetContext(TalkRequest call) {
        talksConfig.getTalkManager().resetTalkContexts(call.getChatId());
    }
}
