package org.app.buttons;

import java.util.Optional;
import org.logicas.librerias.talks.api.TalkType;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.engine.InputHandler;
import org.logicas.librerias.talks.engine.InputTrigger;
import org.logicas.librerias.talks.engine.Talk;
import org.logicas.librerias.talks.engine.TalkRequest;
import org.logicas.librerias.talks.engine.TalkResponse;

public class Buttons extends Talk {

    public Buttons(TalksConfiguration talksConfig, TalkType talkType) {
        super(talksConfig, talkType);
        createContext();
    }

    @Override
    protected Optional<InputHandler> getDefaultHandler() {
        return Optional.of(InputHandler.of("buttons.Buttons::menu", InputTrigger.ofString("/buttons")));
    }

    public TalkResponse menu(TalkRequest call) {
        return TalkResponse.ofText(
            call.getChatId(),
            "Buttons" +
                "\n/buttons" +
                "\n/start (reset bot)"
        )
        .withButton("buttons.Buttons::menu", "1")
        .withButton("buttons.Buttons::menu", "2")
        .withButton("buttons.Buttons::menu", "3")
        .withButton("buttons.Buttons::menu", "4")
        .withButton("buttons.Buttons::menu", "4")
        .withButton("buttons.Buttons::menu", "4")
        .withButton("buttons.Buttons::menu", "4")
        .withButton("buttons.Buttons::menu", "8")
        .withButton("buttons.Buttons::menu", "8")
        .withButton("buttons.Buttons::menu", "8")
        .withButton("buttons.Buttons::menu", "8")
        .withButton("buttons.Buttons::menu", "C")
        .withButton("buttons.Buttons::menu", "13")
        .withButton("buttons.Buttons::menu", "14")
        .withButton("buttons.Buttons::menu", "mamma mia")
        .withButton("buttons.Buttons::menu", "R0C1", 0, 1)
        .withButton("buttons.Buttons::menu", "R8C4", 8, 4)
        ;
    }
}
