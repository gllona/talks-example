package org.app.groups;

import java.util.Optional;
import org.logicas.librerias.talks.api.TalkType;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.engine.InputHandler;
import org.logicas.librerias.talks.engine.InputTrigger;
import org.logicas.librerias.talks.engine.InputTrigger.GroupAction;
import org.logicas.librerias.talks.engine.Talk;
import org.logicas.librerias.talks.engine.TalkRequest;
import org.logicas.librerias.talks.engine.TalkResponse;

public class JoinGroup extends Talk {

    public JoinGroup(TalksConfiguration talksConfig, TalkType talkType) {
        super(talksConfig, talkType);
        createContext();
    }

    @Override
    protected Optional<InputHandler> getDefaultHandler() {
        return Optional.of(InputHandler.of("groups.JoinGroup::join", InputTrigger.ofGroupAction(GroupAction.JOIN)));
    }

    public TalkResponse join(TalkRequest call) {
        return TalkResponse.ofText(
            call.getChatId(),
            "Bot joined group!"
        );
    }
}
