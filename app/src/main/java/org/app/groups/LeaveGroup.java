package org.app.groups;

import java.util.Optional;
import org.logicas.librerias.talks.api.TalkType;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.engine.InputHandler;
import org.logicas.librerias.talks.engine.InputTrigger;
import org.logicas.librerias.talks.engine.InputTrigger.GroupAction;
import org.logicas.librerias.talks.engine.Talk;
import org.logicas.librerias.talks.engine.TalkRequest;
import org.logicas.librerias.talks.engine.TalkResponse;

public class LeaveGroup extends Talk {

    public LeaveGroup(TalksConfiguration talksConfig, TalkType talkType) {
        super(talksConfig, talkType);
        createContext();
    }

    @Override
    protected Optional<InputHandler> getDefaultHandler() {
        return Optional.of(InputHandler.of("groups.LeaveGroup::leave", InputTrigger.ofGroupAction(GroupAction.LEAVE)));
    }

    public TalkResponse leave(TalkRequest call) {
        return TalkResponse.ofText(
            call.getChatId(),
            "Bot left group!"
        );
    }
}
