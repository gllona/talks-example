package org.app.communicator;

import org.logicas.librerias.talks.api.TalkType;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.engine.InputHandler;
import org.logicas.librerias.talks.engine.InputTrigger;
import org.logicas.librerias.talks.engine.Talk;

import java.util.Optional;

public class ContactTalk extends Talk {

    public ContactTalk(TalksConfiguration talksConfig, TalkType talkType) {
        super(talksConfig, talkType);
        createContext("Main");
    }

    @Override
    protected Optional<InputHandler> getDefaultHandler() {
        return Optional.of(
            InputHandler.of("communicator.CommunicatorTalk::writeToOperators",
                InputTrigger.ofString("/contact"))
        );
    }
}
