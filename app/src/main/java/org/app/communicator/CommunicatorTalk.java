package org.app.communicator;

import com.pengrad.telegrambot.model.Chat;
import org.app.Area;
import org.app.Triggers;
import org.app.admin.AuthService;
import org.logicas.librerias.talks.api.TalkType;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.engine.*;

import java.util.Optional;

public class CommunicatorTalk extends Talk {

    private static final String OPERATOR = "Operator";
    private static final String OPERATORS = "Operators";
    private static final String EXIT_CLASS_AND_METHOD = "main.Main::main";

    private AuthService authService = AuthService.getInstance(talksConfig);
    private CommunicatorService communicatorService = CommunicatorService.getInstance(talksConfig);

    public CommunicatorTalk(TalksConfiguration talksConfig, TalkType talkType) {
        super(talksConfig, talkType);
        createContext("Main");
    }

    @Override
    public Optional<InputHandler> getDefaultHandler() {
        return Optional.of(
            InputHandler.of("communicator.CommunicatorTalk::write",
                InputTrigger.ofRegex("/reply_?([0-9]+)?_?([A-Za-z0-9_]+)?"))
        );
    }

    public TalkResponse write(TalkRequest request) {
        String targetChatIdStr = request.getMatch().getRegexMatches().get(1);
        String targetTgUserId = request.getMatch().getRegexMatches().get(2);

        if (targetChatIdStr != null) {
            if (authService.userCanCommunicateWithOtherUsers(request.getChatId())) {
                long targetChatId;
                try {
                    targetChatId = Long.parseLong(targetChatIdStr);
                } catch (NumberFormatException e) {
                    return unauthorizedOperation(request);
                }
                return writeToUser(request, targetChatId, targetTgUserId);
            } else {
                return unauthorizedOperation(request);
            }
        } else {
            return writeToOperators(request);
        }
    }

    private TalkResponse writeToUser(TalkRequest request, long targetChatId, String targetTgUserId) {
        getContext().set(request.getChatId(), "TARGET_CHAT_ID", targetChatId);
        getContext().set(request.getChatId(), "TARGET_TG_USER_ID", targetTgUserId);

        return TalkResponse.ofText(
            request.getChatId(),
            "Enter your response to the user in a single message:"
        ).withRegex("communicator.CommunicatorTalk::doWriteToUser", Triggers.NOT_A_COMMAND);
    }

    public TalkResponse doWriteToUser(TalkRequest request) {
        Area area = Area.from(
            getContext().getString(request.getChatId(), "AREA")
        );
        Long targetChatId = getContext().getLong(request.getChatId(), "TARGET_CHAT_ID");
        String targetTgUserIdVarValue = getContext().getString(request.getChatId(), "TARGET_TG_USER_ID");
        String targetTgUserId = targetTgUserIdVarValue == null ? null : "@" + targetTgUserIdVarValue;
        String targetScope = getContext().getString(targetChatId, "AREA");
        String operatorTgUserId = "@" + request.getMessage().chat().username();

        communicatorService.sendToOperatorsBoxed(request, area, Optional.of(request.getChatId()),
            OPERATORS.toUpperCase() + " WROTE TO USER"
                + (targetScope == null ? "" : " (" + targetScope + ")"),
            request.getMessage().text(),
            "END OF MESSAGE",
            "To reply: /reply_" + targetChatId + (targetTgUserIdVarValue == null ? "" : "_" + targetTgUserIdVarValue)
                + (targetTgUserId == null ? "" : " o " + targetTgUserId)
                + ". Or talk to the " + OPERATOR + ": " + operatorTgUserId
        );

        communicatorService.sendToUser(
            request,
            targetChatId,
            "MESSAGE FROM " + OPERATORS.toUpperCase(),
            request.getMessage().text(),
            "END OF MESSAGE",
            "To reply to " + OPERATORS + " please tap on /reply",
            true
        );

        return TalkResponse.ofText(
            request.getChatId(),
            "The response has been sent to the user\n"
                + "\n"
                + "You can send another reply with /reply_" + targetChatId
        );
    }

    public TalkResponse writeToOperators(TalkRequest request) {
        String scopeStr = getContext().getString(request.getChatId(), "AREA");
        Area country = Area.from(scopeStr);

        return TalkResponse.ofText(
            request.getChatId(),
            "📥 You can send us any questions or comments here.\n"
                + "\n"
                + "Please write a single message with your question. Or go back to the main /menu"
        ).withString(EXIT_CLASS_AND_METHOD, "/menu")
         .withRegex("communicator.CommunicatorTalk::doWriteToOperators", Triggers.NOT_A_COMMAND);
    }

    public TalkResponse doWriteToOperators(TalkRequest request) {
        String scopeStr = getContext().getString(request.getChatId(), "AREA");
        Area country = Area.from(scopeStr);

        Chat chat = request.getMessage().chat();
        String tgUsername = chat.username() == null ? null : "@" + chat.username();
        String tgFullName = ((chat.firstName() == null ? "" : chat.firstName().trim()) + " " +
            (chat.lastName() == null ? "" : chat.lastName().trim())).trim();
        if (tgFullName.isBlank()) {
            tgFullName = "N/A";
        }

        communicatorService.sendToOperatorsBoxed(request, country,
            "MESSAGE FROM USER\n"
                + tgFullName
                + (scopeStr == null ? "" : " (" + scopeStr + ")")
                + "\n"
                + "Has submitted the following comment or question:",
            request.getMessage().text(),
            "END OF MESSAGE",
            "To reply: /reply_" + request.getChatId() + (chat.username() == null ? "" : "_" + chat.username())
                + (tgUsername == null ? "" : " or " + tgUsername)
        );

        return TalkResponse.ofText(
            request.getChatId(),
            "Thanks. If it's a question, one of us will contact you soon.\n"
                + "\n"
                + "To send us another message tap on /contact\n"
                + "\n"
                + "* Back to the main /menu"
        ).withString(EXIT_CLASS_AND_METHOD, "/menu");
    }

    private TalkResponse unauthorizedOperation(TalkRequest request) {
        return TalkResponse.ofText(
            request.getChatId(),
            "You do not have privileges for this operation\n"
                + "\n"
                + "Back to the /menu"
        ).withString(EXIT_CLASS_AND_METHOD, "/menu");
    }
}
