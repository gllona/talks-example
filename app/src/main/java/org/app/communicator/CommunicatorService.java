package org.app.communicator;

import org.app.Area;
import org.app.admin.Role;
import org.app.admin.User;
import org.app.admin.UserRepository;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.engine.TalkRequest;
import org.logicas.librerias.talks.engine.TalkResponse;

import java.io.File;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.Collections.singletonList;

public class CommunicatorService {

    static final int TG_MESSAGE_LINE_MAX_CHARS = 24;   // 30 in 1080p phones

    private static CommunicatorService instance;

    private final TalksConfiguration talksConfig;
    private final TextWrapperService textWrapperService;
    private final UserRepository userRepository;

    public static synchronized CommunicatorService getInstance(TalksConfiguration talksConfig) {
        if (instance == null) {
            instance = new CommunicatorService(talksConfig);
        }
        return instance;
    }

    public CommunicatorService(TalksConfiguration talksConfig) {
        this.talksConfig = talksConfig;
        this.textWrapperService = TextWrapperService.getInstance(TG_MESSAGE_LINE_MAX_CHARS);
        this.userRepository = UserRepository.getInstance(talksConfig.getDao());
    }

    public void sendToOperatorsUnboxed(TalkRequest request, Area country,
                                       String header, String message, String footer, String postFooter
    ) {
        sendToOperators(request, country, Optional.empty(), header, message, footer, postFooter, false);
    }

    public void sendToOperatorsBoxed(TalkRequest request, Area country,
                                     String header, String message, String footer, String postFooter
    ) {
        sendToOperatorsBoxed(request, country, Optional.empty(), header, message, footer, postFooter);
    }

    public void sendToOperatorsBoxed(TalkRequest request, Area country,
                                     Optional<Long> exceptThisOperatorChatId,
                                     String header, String message, String footer, String postFooter
    ) {
        sendToOperators(request, country, exceptThisOperatorChatId, header, message, footer, postFooter, true);
    }

    private void sendToOperators(TalkRequest request, Area country,
                                 Optional<Long> exceptThisOperatorChatId,
                                 String header, String message, String footer, String postFooter,
                                 boolean boxed
    ) {
        List<Long> operators = getOperatorsChatIds(country, exceptThisOperatorChatId);

        sendTo(request, operators, header, message, footer, postFooter, boxed);
    }

    public void sendToOperators(TalkRequest request, Area country, File file) {
        sendToOperators(request, country, Optional.empty(), file);
    }

    public void sendToOperators(TalkRequest request, Area country, Optional<Long> exceptThisOperatorChatId, File file) {
        List<Long> operators = getOperatorsChatIds(country, exceptThisOperatorChatId);

        sendTo(request, operators, file);
    }

    private List<Long> getOperatorsChatIds(Area country, Optional<Long> exceptThisOperatorChatId) {
        List<User> allOperators = userRepository.readByRoleAndScopeWithTgChatIdSet(Role.OPERATOR, country == null ? null : country.name());
        List<Long> operators = allOperators.stream()
            .filter(operator -> exceptThisOperatorChatId.isEmpty() || ! operator.getTgChatId().equals(exceptThisOperatorChatId.get()))
            .map(User::getTgChatId)
            .collect(Collectors.toList());
        return operators;
    }

    public void sendToUser(TalkRequest request, long targetChatId,
                           String header, String message, String footer, String postFooter,
                           boolean boxed
    ) {
        sendTo(request, singletonList(targetChatId), header, message, footer, postFooter, boxed);
    }

    private void sendTo(TalkRequest request, List<Long> chatIds,
                        String header, String message, String footer, String postFooter,
                        boolean boxed
    ) {
        String fullMessage = Stream.of(
            Optional.ofNullable(header),
            Optional.of(""),
            Optional.ofNullable(message),
            Optional.of(""),
            Optional.ofNullable(footer)
        ).filter(Optional::isPresent)
         .map(Optional::get)
         .collect(Collectors.joining("\n"));
        List<String> fullMessageParts = buildMessageParts(fullMessage, boxed);

        for (long chatId : chatIds) {
            for (String part : fullMessageParts) {
                request.getInputAdapter().sendResponse(
                    TalkResponse.ofHtml(
                        chatId,
                        part
                    ).withPreserveHandlers()
                );
            }
            if (postFooter != null) {
                request.getInputAdapter().sendResponse(
                    TalkResponse.ofText(
                        chatId,
                        postFooter
                    ).withPreserveHandlers()
                );
            }
        }
    }

    private void sendTo(TalkRequest request, List<Long> chatIds, File file) {
        for (long chatId : chatIds) {
            request.getInputAdapter().sendResponse(
                TalkResponse.ofDocument(
                    chatId,
                    file
                ).withPreserveHandlers()
            );
        }
    }

    public void sendToAdmins(TalkRequest request, String message) {
        List<User> admins = userRepository.readByRoleAndScopeWithTgChatIdSet(Role.ADMIN, null);
        List<String> boxedMessageParts = buildMessageParts(message, true);

        for (User admin : admins) {
            for (String part : boxedMessageParts) {
                request.getInputAdapter().sendResponse(
                    TalkResponse.ofHtml(
                        admin.getTgChatId(),
                        part
                    ).withPreserveHandlers()
                );
            }
        }
    }

    private List<String> buildMessageParts(String fullMessage, boolean boxed) {
        if (boxed) {
            return textWrapperService.boxAndSplit(fullMessage);
        } else {
            return textWrapperService.splitToTelegram(fullMessage, true);
        }
    }
}
