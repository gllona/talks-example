package org.app.communicator;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

import static java.util.Collections.singletonList;

public class TextWrapperService {

    @AllArgsConstructor
    private enum BoxChar {
        HORIZONTAL("-"),
        VERTICAL("┇"),
        TOP_LEFT("┎"),
        TOP_RIGHT("┒"),
        BOTTOM_LEFT("┖"),
        BOTTOM_RIGHT("┚");

//        HORIZONTAL("━"),   // doesn't seems to be monospaced in the Telegram app
//        VERTICAL("┃"),
//        TOP_LEFT("┏"),
//        TOP_RIGHT("┓"),
//        BOTTOM_LEFT("┗"),
//        BOTTOM_RIGHT("┛");

        @Getter private final String character;
    }

    public static final int TG_MAX_MESSAGE_CHARS = 4000;   // or 4096
    public static final String TG_HTML_NL = "\n";   // "<pre>\n</pre>"

    private static TextWrapperService instance;

    private final int lineCharLength;

    public static synchronized TextWrapperService getInstance(int lineCharLength) {
        if (instance == null) {
            instance = new TextWrapperService(lineCharLength);
        }
        return instance;
    }

    private TextWrapperService(int lineCharLength) {
        this.lineCharLength = lineCharLength;
    }

    private String boxTextToHtml(String message, boolean addCodeTagPerLine) {
        List<String> lines = new ArrayList<>();
        String[] paragraphs = message.split("\\n");

        lines.add(monospacedHtml(boxHeader(), addCodeTagPerLine));

        for (String paragraph : paragraphs) {
            List<String> splittedParagraph = boxParagraph(paragraph, addCodeTagPerLine);
            lines.addAll(splittedParagraph);
        }

        lines.add(monospacedHtml(boxFooter(), addCodeTagPerLine));

        String joined = String.join(TG_HTML_NL, lines);

        return joined;
    }

    private List<String> boxParagraph(String paragraph, boolean addCodeTagPerLine) {
        if ("".equals(paragraph)) {
            return singletonList(monospacedHtml(boxLine(""), addCodeTagPerLine));
        }

        List<String> lines = new ArrayList<>();
        String[] words = paragraph.split(" ");

        StringBuilder line = new StringBuilder();
        for (String word : words) {
            if (line.length() + " ".length() + word.length() + 4 > lineCharLength) {
                lines.add(monospacedHtml(boxLine(line.toString()), addCodeTagPerLine));
                line = new StringBuilder();
            }
            if (line.length() > 0) {
                line.append(" ");
            }
            line.append(word);
        }
        if (line.length() > 0) {
            lines.add(monospacedHtml(boxLine(line.toString()), addCodeTagPerLine));
        }

        return lines;
    }

    private String boxHeader() {
        return BoxChar.TOP_LEFT.getCharacter()
            + StringUtils.repeat(BoxChar.HORIZONTAL.getCharacter(), lineCharLength - 2)
            + BoxChar.TOP_RIGHT.getCharacter();
    }

    private String boxFooter() {
        return BoxChar.BOTTOM_LEFT.getCharacter()
            + StringUtils.repeat(BoxChar.HORIZONTAL.getCharacter(), lineCharLength - 2)
            + BoxChar.BOTTOM_RIGHT.getCharacter();
    }

    private String boxLine(String line) {
        return BoxChar.VERTICAL.getCharacter() + " "
            + StringUtils.rightPad(line, lineCharLength - 4)
            + " " + BoxChar.VERTICAL.getCharacter();
    }

    private String replaceHtmlEntities(String line) {
        String[] htmlEntities = new String[]{
            "b", "strong", "i", "em", "u", "ins", "s", "strike", "del", "a", "code", "pre"
        };
        for (String htmlEntity : htmlEntities) {
            line = line.replaceAll("<" + htmlEntity + ">", "{{" + htmlEntity + "}}")
                       .replaceAll("</" + htmlEntity + ">", "{{/" + htmlEntity + "}}");
        }
        line = line.replace("&", "&amp;")
                   .replace("<", "&lt;")
                   .replace(">", "&gt;");
        for (String htmlEntity : htmlEntities) {
            line = line.replaceAll("\\{\\{" + htmlEntity + "}}", "<" + htmlEntity + ">")
                       .replaceAll("\\{\\{/" + htmlEntity + "}}", "</" + htmlEntity + ">");
        }
        return line;
    }

    private String monospacedHtml(String line, boolean addCodeTagPerLine) {
        String finalLine = replaceHtmlEntities(line);

        return addCodeTagPerLine ?
            "<code>" + finalLine + "</code>" :
            finalLine;
    }

    public List<String> splitToTelegram(String boxedMessage, boolean replaceHtmlEntities) {
        List<String> splitted = new ArrayList<>();
        String[] lines = boxedMessage.split("\\n|<br/?>|<pre>\\n</pre>");

        StringBuilder currentPart = new StringBuilder();
        for (String line : lines) {
            if (currentPart.length() + TG_HTML_NL.length() + line.length() + 4 > TG_MAX_MESSAGE_CHARS) {
                splitted.add(currentPart.toString());
                currentPart = new StringBuilder();
            }
            if (currentPart.length() > 0) {
                currentPart.append(TG_HTML_NL);
            }
            currentPart.append(replaceHtmlEntities ? replaceHtmlEntities(line) : line);
        }
        if (currentPart.length() > 0) {
            splitted.add(currentPart.toString());
        }

        return splitted;
    }

    public List<String> boxAndSplit(String message) {
        String boxedText = "<pre>" + boxTextToHtml(message, false) + "</pre>";

        return boxedText.length() <= TG_MAX_MESSAGE_CHARS ?
            singletonList(boxedText) :
            splitToTelegram(boxTextToHtml(message, true), false);
    }
}
