package org.app.xarxa;

import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import org.app.xarxa.XarxaHelper.TipoBusqueda;
import org.logicas.librerias.talks.api.TalkType;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.engine.InputHandler;
import org.logicas.librerias.talks.engine.InputTrigger;
import org.logicas.librerias.talks.engine.Talk;
import org.logicas.librerias.talks.engine.TalkRequest;
import org.logicas.librerias.talks.engine.TalkResponse;

public class XarxaOfrecimientos extends Talk {

    // simulacion
    boolean esAdministrador = true;

    private static XarxaHelper helper;

    public XarxaOfrecimientos(TalksConfiguration talksConfig, TalkType talkType) {
        super(talksConfig, talkType);
        createContext("Xarxa");
        helper = XarxaHelper.getInstance(talksConfig.getDao());
    }

    @Override
    protected Optional<InputHandler> getDefaultHandler() {
        return Optional.of(InputHandler.of("xarxa.XarxaOfrecimientos::menu", InputTrigger.ofString("/ofrecer")));
    }

    // MENU PRINCIPAL

    public TalkResponse menu(TalkRequest call) {
        return TalkResponse.ofText(
            call.getChatId(),
            "/crear nuevo ofrecimiento" +
                "\nVer /mis ofrecimientos" +
                "\n/buscar ofrecimientos"
        )
        .withString("xarxa.XarxaOfrecimientos::nuevoOfrecimiento", "/crear")
        .withString("xarxa.XarxaOfrecimientos::misOfrecimientos", "/mis")
        .withString("xarxa.XarxaOfrecimientos::buscarOfrecimiento", "/buscar");
    }

    // NUEVO OFRECIMIENTO

    public TalkResponse nuevoOfrecimiento(TalkRequest call) {
        Map<String, String> categorias = helper.getCategorias();
        String textoCategorias = categorias.entrySet().stream()
            .map(entry -> "* /" + entry.getKey() + " " + entry.getValue())
            .collect(Collectors.joining("\n"));

        return TalkResponse.ofText(
            call.getChatId(),
            "Selecciona una categoría según lo que puedes aportar:\n" +
                textoCategorias +
                (esAdministrador ? "\n/menu principal" : "")
            )
            .withRegex("xarxa.XarxaOfrecimientos::agregarDescripcion", "/(.*)");
    }

    public TalkResponse agregarDescripcion(TalkRequest call) {
        String slugCategoria = call.getMatch().getRegexMatches().get(1);
        String descripcionCategoria = helper.getCategorias().get(slugCategoria);

        if (descripcionCategoria == null) {
            return TalkResponse.ofText(
                call.getChatId(),
                "No se ha reconocido la categoria" +
                    "\nVolver a seleccionar /categoria"
                )
                .withString("xarxa.XarxaOfrecimientos::seleccionarCategoria", "/categoria");
        }

        return TalkResponse.ofText(
            call.getChatId(),
            "Has seleccionado ofrecer:" +
                "\n* " + descripcionCategoria +
                "\nIntroduce detalles de tu aporte (zona de cobertura, condiciones, modalidad, etc.):"
            )
            .withRegex("xarxa.XarxaOfrecimientos::guardarOfrecimiento", "(.*)");
    }

    public TalkResponse guardarOfrecimiento(TalkRequest call) {
        return TalkResponse.ofText(
            call.getChatId(),
            "Se ha guardado tu ofrecimiento y los coordinadores han sido notificados" +
                "\nOfrecimiento: /ofrecimiento_" +
                    helper.getHashidOfrecimiento() +
                "\n**Gracias por apoyar!**" +
                "\n/menu principal"
            );
    }

    // MIS OFRECIMIENTOS

    public TalkResponse misOfrecimientos(TalkRequest call) {
        Map<String, String> ofrecimientos = helper.getMisOfrecimientos();
        String textoOfrecimientos = ofrecimientos.entrySet().stream().map(
            entry -> "* /ofrecimiento_" + entry.getKey() + " " + entry.getValue()
        ).collect(Collectors.joining("\n"));

        return TalkResponse.ofText(
            call.getChatId(),
            "Estos son tus ofrecimientos:" +
                "\n\n" + textoOfrecimientos +
                "\n\n(Selecciona un ofrecimiento para tenerlo en memoria)" +
                "\n/ofrecer soporte" +
                "\n/menu principal"
            )
            .withRegex("xarxa.XarxaOfrecimientos::ofrecimiento", "/ofrecimiento_([A-Z0-9]+)");
    }

    // BUSCAR OFRECIMIENTOS

    public TalkResponse buscarOfrecimiento(TalkRequest call) {
        helper.resetearCriterio(TipoBusqueda.OFRECIMIENTO, call.getChatId(), this);
        return criterioBusqueda(call);
    }

    public TalkResponse criterioBusqueda(TalkRequest call) {
        String criterio = helper.construirTextoCriterio(TipoBusqueda.OFRECIMIENTO, call.getChatId(), this);

        return TalkResponse.ofText(
            call.getChatId(),
            "Fijar criterios:" +
                "\n* /nombre de persona" +
                "\n* Usuario de /telegram" +
                "\n* /categoria de soporte" +
                "\n* Rango de /fechas" +
                "\n* Si tiene soportes /asignados" +
                "\n\nEl criterio actual de busqueda es:" +
                "\n" + (criterio.isEmpty() ? "NO HAY CRITERIOS FIJADOS" : criterio) +
                "\n\n/limpiar criterios de búsqueda" +
                "\n/buscar según los criterios fijados"
            )
            .withString("xarxa.XarxaOfrecimientos::criterioNombre", "/nombre")
            .withString("xarxa.XarxaOfrecimientos::criterioTelegram", "/telegram")
            .withString("xarxa.XarxaOfrecimientos::criterioCategoria", "/categoria")
            .withString("xarxa.XarxaOfrecimientos::criterioFechas", "/fechas")
            .withString("xarxa.XarxaOfrecimientos::criterioConSoportes", "/asignados")
            .withString("xarxa.XarxaOfrecimientos::limpiarCriterio", "/limpiar")
            .withString("xarxa.XarxaOfrecimientos::efectuarBusqueda", "/buscar");
    }

    public TalkResponse limpiarCriterio(TalkRequest call) {
        call.getInputAdapter().sendResponse(TalkResponse.ofText(call.getChatId(),
            "Se han limpiado los criterios de búsqueda"
        ));
        return buscarOfrecimiento(call);
    }

    public TalkResponse criterioNombre(TalkRequest call) {
        return TalkResponse.ofText(
            call.getChatId(),
            "Introduce el nombre de la persona o una parte del nombre:" +
                "\nIr a /criterios"
        )
        .withRegex("xarxa.XarxaOfrecimientos::criterioNombreFijar", ".*");
    }

    public TalkResponse criterioNombreFijar(TalkRequest call) {
        if (call.getMatch().getRegexMatches().get(0).equalsIgnoreCase("/criterios")) {
            return criterioBusqueda(call);
        }

        getContext().set(call.getChatId(), "ofrecimiento_nombre", call.getMatch().getRegexMatches().get(0));
        call.getInputAdapter().sendResponse(TalkResponse.ofText(call.getChatId(),
            "Se ha asignado el criterio de búsqueda por nombre de persona"
        ));
        return criterioBusqueda(call);
    }

    public TalkResponse criterioTelegram(TalkRequest call) {
        return TalkResponse.ofText(
            call.getChatId(),
            "Introduce el usuario de Telegram de la persona:" +
                "\nIr a /criterios"
        )
        .withRegex("xarxa.XarxaOfrecimientos::criterioTelegramFijar", ".*");
    }

    public TalkResponse criterioTelegramFijar(TalkRequest call) {
        if (call.getMatch().getRegexMatches().get(0).equalsIgnoreCase("/criterios")) {
            return criterioBusqueda(call);
        }

        call.getInputAdapter().sendResponse(TalkResponse.ofText(call.getChatId(),
            "Se ha asignado el criterio de búsqueda por usuario de Telegram"
        ));
        getContext().set(call.getChatId(), "ofrecimiento_telegram", call.getMatch().getRegexMatches().get(0));
        return criterioBusqueda(call);
    }

    public TalkResponse criterioCategoria(TalkRequest call) {
        Map<String, String> categorias = helper.getCategorias();
        String textoCategorias = categorias.entrySet().stream()
            .map(entry -> "* /" + entry.getKey() + " " + entry.getValue())
            .collect(Collectors.joining("\n"));

        return TalkResponse.ofText(
            call.getChatId(),
            "Selecciona una categoría por la que quieras filtrar:\n" +
                textoCategorias +
                "\n\nIr a /criterios"
        )
       .withRegex("xarxa.XarxaOfrecimientos::criterioCategoriaFijar", "/(.*)");
    }

    public TalkResponse criterioCategoriaFijar(TalkRequest call) {
        if (call.getMatch().getRegexMatches().get(0).equalsIgnoreCase("/criterios")) {
            return criterioBusqueda(call);
        }

        getContext().set(call.getChatId(), "ofrecimiento_categoria", call.getMatch().getRegexMatches().get(1));
        call.getInputAdapter().sendResponse(TalkResponse.ofText(call.getChatId(),
            "Se ha asignado el criterio de búsqueda por categoría"
        ));
        return criterioBusqueda(call);
    }

    public TalkResponse criterioFechas(TalkRequest call) {
        return TalkResponse.ofText(
            call.getChatId(),
            "Introduce la fecha inicial en formato DD-MM-AAAA (también puedes introducir /hoy o /ayer):" +
                "\nIr a /criterios"
        )
        .withRegex("xarxa.XarxaOfrecimientos::criterioFechas2", XarxaHelper.REGEX_FECHA_CRITERIOS);
    }

    public TalkResponse criterioFechas2(TalkRequest call) {
        if (call.getMatch().getRegexMatches().get(0).equalsIgnoreCase("/criterios")) {
            return criterioBusqueda(call);
        }

        String fechaDesde = helper.construirFecha(call.getUpdate().message().text(), call.getMatch().getRegexMatches(), 1, false, true);
        getContext().set(call.getChatId(), "ofrecimiento_fechaDesde", fechaDesde);
        return TalkResponse.ofText(
            call.getChatId(),
            "Introduce la fecha final en formato DD-MM-AAAA (también puedes introducir /hoy o /ayer o escribe /ok para usar la misma fecha inicial):" +
                "\nIr a /criterios"
        )
        .withRegex("xarxa.XarxaOfrecimientos::criterioFechasFijar", XarxaHelper.REGEX_FECHA_CRITERIOS_OK);
    }

    public TalkResponse criterioFechasFijar(TalkRequest call) {
        if (call.getMatch().getRegexMatches().get(0).equalsIgnoreCase("/criterios")) {
            return criterioBusqueda(call);
        }

        String fechaHasta = call.getMatch().getRegexMatches().get(0).equalsIgnoreCase("/ok") ?
            getContext().getString(call.getChatId(), "ofrecimiento_fechaDesde") :
            helper.construirFecha(call.getUpdate().message().text(), call.getMatch().getRegexMatches(), 1, false, true);
        getContext().set(call.getChatId(), "ofrecimiento_fechaHasta", fechaHasta);

        call.getInputAdapter().sendResponse(TalkResponse.ofText(call.getChatId(),
            "Se ha asignado el criterio de búsqueda por rango de fechas"
        ));
        return criterioBusqueda(call);
    }

    public TalkResponse criterioConSoportes(TalkRequest call) {
        return TalkResponse.ofText(
            call.getChatId(),
            "Indica si quieres filtrar según si los ofrecimientos tienen asignados soportes o no:" +
                "\n/si" +
                "\n/no" +
                "\n/indiferente" +
                "\n\nIr a /criterios"
        )
        .withRegex("xarxa.XarxaOfrecimientos::criterioConSoportesFijar", "/(.*)");
    }

    public TalkResponse criterioConSoportesFijar(TalkRequest call) {
        if (call.getMatch().getRegexMatches().get(0).equalsIgnoreCase("/criterios")) {
            return criterioBusqueda(call);
        }

        String conSoportes = call.getMatch().getRegexMatches().get(1);
        if (conSoportes.equalsIgnoreCase("indiferente")) {
            conSoportes = null;
        }
        getContext().set(call.getChatId(), "ofrecimiento_conSoportes", conSoportes);

        call.getInputAdapter().sendResponse(TalkResponse.ofText(call.getChatId(),
            "Se ha asignado el criterio de búsqueda por si hay asignados o no soportes"
        ));
        return criterioBusqueda(call);
    }

    public TalkResponse efectuarBusqueda(TalkRequest call) {
        Map<String, String> ofrecimientos = helper.getOfrecimientosBuscados();
        String textoOfrecimientos = ofrecimientos.entrySet().stream().map(
            entry -> "* /ofrecimiento_" + entry.getKey() + " " + entry.getValue()
        ).collect(Collectors.joining("\n"));

        return TalkResponse.ofText(
            call.getChatId(),
            "Estos son los ofrecimientos encontrados según el criterio fijado:" +
                "\n\n" + textoOfrecimientos +
                "\n\n(Selecciona un ofrecimiento para tenerlo en memoria)" +
                "\nContinuar la /busqueda" +
                "\n/menu principal"
        )
        .withString("xarxa.XarxaOfrecimientos::criterioBusqueda", "/busqueda");
    }

    // FICHA OFRECIMIENTO

    public TalkResponse ofrecimiento(TalkRequest call) {
        String hashidOfrecimiento = call.getMatch().getRegexMatches().get(1);
        String fichaOfrecimiento = helper.getFichaOfrecimiento(hashidOfrecimiento);

        if (fichaOfrecimiento != null) {
            getContext().set(call.getChatId(), "hashid_ofrecimiento", hashidOfrecimiento);
            call.getInputAdapter().sendResponse(TalkResponse.ofText(call.getChatId(),
                fichaOfrecimiento
            ));
        }
        return TalkResponse.ofText(
            call.getChatId(),
            (fichaOfrecimiento == null ?
                    "NO SE HA ENCONTRADO UN OFRECIMIENTO CON EL CODIGO INDICADO" :
                    "(Se ha guardado el ofrecimiento en la memoria)") +
                "\n/ofrecer soporte" +
                (esAdministrador ? "\nGestionar /soportes" : "") +
                "\n/menu principal"
            )
            .withPreserveHandlers();
    }
}
