package org.app.xarxa;

import static java.util.Arrays.asList;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.logicas.librerias.talks.engine.Talk;
import org.logicas.librerias.copersistance.Dao;

@AllArgsConstructor
public class XarxaHelper {

    @AllArgsConstructor
    @Getter
    public enum TipoBusqueda {
        PERSONA("persona"),
        OFRECIMIENTO("ofrecimiento"),
        PETICION("peticion"),
        SOPORTE("soporte");
        private String nombre;
    }

    public static final String REGEX_FECHA = "([0-9]{2})[-/]([0-9]{2})[-/]([0-9]{4})|/hoy|/ayer";
    public static final String REGEX_FECHA_CRITERIOS = REGEX_FECHA + "|/criterios";
    public static final String REGEX_FECHA_CRITERIOS_OK = REGEX_FECHA + "|/criterios|/ok";
    private static XarxaHelper instance;
    private final Dao dao;
    public static synchronized XarxaHelper getInstance(Dao dao) {
        if (instance == null) {
            instance = new XarxaHelper(dao);
        }
        return instance;
    }

    // CATEGORIAS

    public Map<String, String> getCategorias() {
        Map<String, String> categorias = new HashMap<>();
        categorias.put("comida", "Ración de comida");
        categorias.put("transporte", "Llevar cosas de un sitio a otro");
        categorias.put("medicinas", "Comprar medicinas y llevarlas a quien las necesita (quien aporta el dinero)");
        categorias.put("compra", "Comprar en el mercado y llevarlo a quien las necesita (quien aporta el dinero)");
        return categorias;
    }

    // PERSONAS

    public Map<String, String> getPersonasBuscadas() {
        Map<String, String> personas = new HashMap<>();
        personas.put("P9988776", "Petra Guinand @telegramdepetra");
        personas.put("P1122334", "Mikel Faría @telegramdemikel");
        return personas;
    }

    public String getHashidPersona() {
        return "P9988776";
    }

    public String getHashidMiPersona() {
        return "P1122334";
    }

    public String getFichaPersona(String hashidPersona) {
        if (hashidPersona.equals("P9988776")) {
            return "/persona_" + hashidPersona +
                "\nNombre: Petra Guinand" +
                "\nTelegram: @telegramdepetra" +
                "\nTeléfono móvil: 909554433" +
                "\nTeléfono fijo: 609888888" +
                "\nDirección: Calle Sant Pere Mitjà, 44, 1-1" +
                "\nEdad: 55 años";
        } else if (hashidPersona.equals("P1122334")) {
            return "/persona_" + hashidPersona +
                "\nNombre: Mikel Faría" +
                "\nTelegram: @telegramdemikel" +
                "\nTeléfono móvil: 909123451" +
                "\nTeléfono fijo: (no tiene)" +
                "\nDirección: Calle Sant Pere Màs Baix, 11, portal 2" +
                "\nEdad: 35 años";
        } else {
            return null;
        }
    }

    public Map<String, String> getAtributosPersona(String hashidPersona) {
        Map<String, String> atributos = new HashMap<>();
        if (hashidPersona.equals("P9988776")) {
            atributos.put("nombre", "Petra Guinand");
            atributos.put("chatIdTelegram", "142999857");
            atributos.put("telefonoMovil", "909554433");
            atributos.put("telefonoFijo", "609888888");
            atributos.put("direccion", "Calle Sant Pere Mitjà, 44, 1-1");
            atributos.put("fechaNacimiento", "1965-06-01");
        } else if (hashidPersona.equals("P1122334")) {
            atributos.put("nombre", "Mikel Faría");
            atributos.put("chatIdTelegram", "857555142");
            atributos.put("telefonoMovil", "909123451");
            atributos.put("telefonoFijo", null);
            atributos.put("direccion", "Calle Sant Pere Màs Baix, 11, portal 2");
            atributos.put("fechaNacimiento", "1985-08-02");
        }
        return atributos;
    }

    public List<String> getIndiceAtributosPersona() {
        return asList(
            "nombre",
            "chatIdTelegram",
            "telefonoMovil",
            "telefonoFijo",
            "direccion",
            "fechaNacimiento"
        );
    }

    // OFRECIMIENTOS

    public Map<String, String> getMisOfrecimientos() {
        Map<String, String> ofrecimientos = new HashMap<>();
        ofrecimientos.put("XG87UY32", "comida / ofrecido el 2020-04-10");
        ofrecimientos.put("UY99XA21", "compra / ofrecido el 2020-04-08");
        return ofrecimientos;
    }

    public Map<String, String> getOfrecimientosBuscados() {
        Map<String, String> ofrecimientos = new HashMap<>();
        ofrecimientos.put("XG87UY32", "comida / ofrecido el 2020-04-10");
        return ofrecimientos;
    }

    public String getHashidOfrecimiento() {
        return "UY99XA21";
    }

    public String getFichaOfrecimiento(String hashidOfrecimiento) {
        if (hashidOfrecimiento.equals("XG87UY32")) {
            return "/ofrecimiento_" + hashidOfrecimiento +
                "\nCategoria: comida" +
                "\nOfrecido en: 2020-04-10" +
                "\nOfrecido por: @gllona Gorka Llona" +
                "\nDescripción: Ofrezco 5 raciones de comida diarias en la zona de El Born, a buscar en C Sant Pere Mitjà";
        } else if (hashidOfrecimiento.equals("UY99XA21")) {
            return "/ofrecimiento_" + hashidOfrecimiento +
                "\nCategoria: compra" +
                "\nOfrecido en: 2020-04-08" +
                "\nOfrecido por: @usuariodetelegram Juana Cruz" +
                "\nDescripción: Me ofrezco a hacer la compra y llevarla a la casa de quien lo requiera. El importe de la compra se hará por transferencia bancaria en cada caso.";
        } else {
            return null;
        }
    }

    // PETICIONES

    public Map<String, String> getMisPeticiones() {
        Map<String, String> peticiones = new HashMap<>();
        peticiones.put("99PET44P", "medicinas / solicitado el 2020-04-12");
        peticiones.put("81432ASK", "compra / solicitado el 2020-04-11");
        peticiones.put("98273JSM", "comida / solicitado el 2020-04-16");
        return peticiones;
    }

    public Map<String, String> getPeticionesBuscadas() {
        Map<String, String> ofrecimientos = new HashMap<>();
        ofrecimientos.put("99PET44P", "medicinas / ofrecido el 2020-04-06");
        return ofrecimientos;
    }

    public String getHashidPeticion() {
        return "81432ASK";
    }

    public String getFichaPeticion(String hashidPeticion) {
        if (hashidPeticion.equals("99PET44P")) {
            return "/peticion_" + hashidPeticion +
                "\nCategoría: medicinas" +
                "\nOfrecido en: 2020-04-12" +
                "\nOfrecido por: @gllona Gorka Llona" +
                "\nDescripción: Necesito alguien que pueda comprarme medicinas sin receta y llevarlas a mi piso";
        } else if (hashidPeticion.equals("81432ASK")) {
            return "/peticion_" + hashidPeticion +
                "\nCategoría: compra" +
                "\nOfrecido en: 2020-04-11" +
                "\nOfrecido por: @otrousuariodetelegram Juan Pérez" +
                "\nDescripción: Estoy necesitando alguien que pueda hacerme la compra cada tres días y llevarla a mi casa en el Gotic";
        } else if (hashidPeticion.equals("98273JSM")) {
            return "/peticion_" + hashidPeticion +
                "\nCategoría: comida" +
                "\nOfrecido en: 2020-04-16" +
                "\nOfrecido por: @tercerusuariodetelegram Mikel López" +
                "\nDescripción: Necesito urgentemente comida para alimentación personal en la zona de el Born";
        } else {
            return null;
        }
    }

    // SOPORTES

    public Map<String, String> getSoportesBuscados() {
        Map<String, String> soportes = new HashMap<>();
        soportes.put("S666OP99", "comida / asignado el 2020-04-01");
        soportes.put("S98765G7", "transporte / asignado el 2020-04-02");
        return soportes;
    }

    public String getHashidSoporte() {
        return "SOP77777";
    }

    public String getFichaSoporte(String hashidSoporte) {
        return "/soporte_" + hashidSoporte +
            "\nCategoría: comida" +
            "\nAsignado en: 2020-04-20" +
            "\nPetición: /peticion_98273JSM" +
            "\nOfrecimiento: /ofrecimiento_XG87UY32" +
            "\nDescripción: Asignación de ración de comida en el Born";
    }

    // BUSQUEDAS

    public void resetearCriterio(TipoBusqueda tipoBusqueda, long session, Talk talk) {
        talk.getContext().set(session, tipoBusqueda.getNombre() + "_nombre", null);
        talk.getContext().set(session, tipoBusqueda.getNombre() + "_telegram", null);
        talk.getContext().set(session, tipoBusqueda.getNombre() + "_categoria", null);
        talk.getContext().set(session, tipoBusqueda.getNombre() + "_fechaDesde", null);
        talk.getContext().set(session, tipoBusqueda.getNombre() + "_fechaHasta", null);
        talk.getContext().set(session, tipoBusqueda.getNombre() + "_conSoportes", null);
        talk.getContext().set(session, tipoBusqueda.getNombre() + "_direccion", null);
    }

    public String construirTextoCriterio(TipoBusqueda tipoBusqueda, Long chatId, Talk talk) {
        StringJoiner partes = new StringJoiner("\n");
        String nombre = talk.getContext().getString(chatId, tipoBusqueda.getNombre() + "_nombre");
        String telegram = talk.getContext().getString(chatId, tipoBusqueda.getNombre() + "_telegram");
        String categoria = talk.getContext().getString(chatId, tipoBusqueda.getNombre() + "_categoria");
        String fechaDesde = talk.getContext().getString(chatId, tipoBusqueda.getNombre() + "_fechaDesde");
        String fechaHasta = talk.getContext().getString(chatId, tipoBusqueda.getNombre() + "_fechaHasta");
        String conSoportes = talk.getContext().getString(chatId, tipoBusqueda.getNombre() + "_conSoportes");
        String direccion = talk.getContext().getString(chatId, tipoBusqueda.getNombre() + "_direccion");

        if (nombre != null) {
            partes.add("* Nombre de persona contiene: " + nombre);
        }
        if (telegram != null) {
            partes.add("* Usuario de telegram es: " + telegram);
        }
        if (categoria != null) {
            partes.add("* Categoría es: " + categoria);
        }
        if (fechaDesde != null && fechaHasta != null) {
            partes.add("* Rango de fechas es: desde " + fechaDesde + " hasta " + fechaHasta);
        }
        if (conSoportes != null) {
            partes.add("* Con soportes: " + conSoportes);
        }
        if (direccion != null) {
            partes.add("* Dirección contiene: " + direccion);
        }

        return partes.toString();
    }

    // FECHAS

    public String construirFecha(String texto, List<String> matches, int indiceInicialMatches, boolean matchesComoYyyyMmDd, boolean salidaComoYyyyMmDd) {
        SimpleDateFormat formatter = new SimpleDateFormat(salidaComoYyyyMmDd ? "yyyy-MM-dd" : "dd-MM-yyyy");
        Calendar yesterdayCal = Calendar.getInstance();
        yesterdayCal.add(Calendar.DATE, -1);
        Date today = new Date();
        Date yesterday = yesterdayCal.getTime();
        if ("hoy".equalsIgnoreCase(texto) || "/hoy".equalsIgnoreCase(texto)) {
            return formatter.format(today);
        } else if ("ayer".equalsIgnoreCase(texto) || "/ayer".equalsIgnoreCase(texto)) {
            return formatter.format(yesterday);
        } else if (salidaComoYyyyMmDd) {
            return String.format("%s-%s-%s", matches.get(3), matches.get(2), matches.get(1));
        } else {
            return String.format("%s-%s-%s", matches.get(1), matches.get(2), matches.get(3));
        }
    }
}
