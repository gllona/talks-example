package org.app.xarxa;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.stream.Collectors;
import org.app.xarxa.XarxaHelper.TipoBusqueda;
import org.logicas.librerias.talks.api.TalkType;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.engine.InputHandler;
import org.logicas.librerias.talks.engine.InputTrigger;
import org.logicas.librerias.talks.engine.Talk;
import org.logicas.librerias.talks.engine.TalkRequest;
import org.logicas.librerias.talks.engine.TalkResponse;

public class XarxaPersonas extends Talk {

    // simulacion
    boolean esAdministrador = true;

    private static XarxaHelper helper;

    public XarxaPersonas(TalksConfiguration talksConfig, TalkType talkType) {
        super(talksConfig, talkType);
        createContext("Xarxa");
        helper = XarxaHelper.getInstance(talksConfig.getDao());
    }

    @Override
    protected Optional<InputHandler> getDefaultHandler() {
        return Optional.of(InputHandler.of("xarxa.XarxaPersonas::menu", InputTrigger.ofString("/personas")));
    }

    // MENU PRINCIPAL

    public TalkResponse menu(TalkRequest call) {
        Object hashidPersona = getContext().get(call.getChatId(), "hashid_persona");
        Object hashidMiPersona = getContext().get(call.getChatId(), "hashid_miPersona");
        return TalkResponse.ofText(
            call.getChatId(),
            (hashidMiPersona == null ?
                    "No tienes un perfil creado en el sistema" :
                    "Tu perfil es /persona_" + hashidMiPersona) +
                "\n" + (hashidMiPersona == null ? "Crear" : "Modificar") + " mi /perfil" +
                (esAdministrador ? "\n/crear perfil de otra persona" : "") +
                (esAdministrador && hashidPersona != null ? "\n/modificar perfil de /persona_" + hashidPersona : "") +
                "\n/buscar persona"
        )
        .withString("xarxa.XarxaPersonas::editarPerfil", "/perfil")
        .withString("xarxa.XarxaPersonas::crearPerfil", "/crear")
        .withString("xarxa.XarxaPersonas::modificarPerfil", "/modificar")
        .withString("xarxa.XarxaPersonas::buscarPersona", "/buscar");
    }

    // PREPARAR EDITAR PERFIL

    public TalkResponse crearPerfil(TalkRequest call) {
        getContext().set(call.getChatId(), "hashid_persona", null);

        List<String> atributos = helper.getIndiceAtributosPersona();
        for (String atributo : atributos) {
            getContext().set(call.getChatId(), "persona_atributo_" + atributo, null);
        }

        call.getInputAdapter().sendResponse(TalkResponse.ofText(call.getChatId(),
            "Crear un nuevo perfil:"
        ));
        return editarPerfil(call);
    }

    public TalkResponse modificarPerfil(TalkRequest call) {
        String hashidPersona = getContext().getString(call.getChatId(), "hashid_persona");

        if (hashidPersona == null) {
            return TalkResponse.ofText(
                call.getChatId(),
                "Primero debes pasar a memoria los datos de una persona" +
                    "\n/buscar persona"
                )
                .withString("XarxaPersonas", "buscarPersona");
        }

        Map<String, String> atributos = helper.getAtributosPersona(hashidPersona);
        for (Map.Entry<String, String> atributo : atributos.entrySet()) {
            getContext().set(call.getChatId(), "persona_atributo_" + atributo.getKey(), atributo.getValue());
        }

        call.getInputAdapter().sendResponse(TalkResponse.ofText(call.getChatId(),
            "Modificar el perfil de /persona_" + hashidPersona + ":"
        ));
        return editarPerfil(call);
    }

    // EDITAR PERFIL DE PERSONA

    public TalkResponse editarPerfil(TalkRequest call) {
        Object hashidPersona = getContext().get(call.getChatId(), "hashid_persona");
        String textoAtributos = getTextoAtributosPersona(call);

        return TalkResponse.ofText(
            call.getChatId(),
            "El perfil " + (hashidPersona == null ? "en creación" : "de /persona_" + hashidPersona) + " es:" +
                "\n" + textoAtributos +
                "\n\nModificar atributos:" +
                "\n* /nombre" +
                "\n* ID de /telegram (numérico, no es igual al nombre de usuario en Telegram)" +
                "\n* Teléfono /movil" +
                "\n* Teléfono /fijo" +
                "\n* /direccion" +
                "\n* Fecha de /nacimiento" +
                "\n\n/guardar perfil de persona"
        )
        .withString("xarxa.XarxaPersonas::editarNombre", "/nombre")
        .withString("xarxa.XarxaPersonas::editarChatIdTelegram", "/telegram")
        .withString("xarxa.XarxaPersonas::editarTelefonoMovil", "/movil")
        .withString("xarxa.XarxaPersonas::editarTelefonoFijo", "/fijo")
        .withString("xarxa.XarxaPersonas::editarDireccion", "/direccion")
        .withString("xarxa.XarxaPersonas::editarFechaNacimiento", "/nacimiento")
        .withString("xarxa.XarxaPersonas::guardarPerfil", "/guardar");
    }

    private String getTextoAtributosPersona(TalkRequest call) {
        String nombre = getContext().getString(call.getChatId(),"persona_atributo_nombre");
        String chatIdTelegram = getContext().getString(call.getChatId(),"persona_atributo_chatIdTelegram");
        String telefonoMovil = getContext().getString(call.getChatId(),"persona_atributo_telefonoMovil");
        String telefonoFijo = getContext().getString(call.getChatId(),"persona_atributo_telefonoFijo");
        String direccion = getContext().getString(call.getChatId(),"persona_atributo_direccion");
        String fechaNacimiento = getContext().getString(call.getChatId(),"persona_atributo_fechaNacimiento");
        StringJoiner atributos = new StringJoiner("\n");
        atributos.add("* Nombre: " + (nombre == null ? "(no se ha fijado)" : nombre));
        atributos.add("* ID de Telegram: " + (chatIdTelegram == null ? "(no se ha fijado)" : chatIdTelegram));
        atributos.add("* Teléfono móvil: " + (telefonoMovil == null ? "(no se ha fijado)" : telefonoMovil));
        atributos.add("* Teléfono fijo: " + (telefonoFijo == null ? "(no se ha fijado)" : telefonoFijo));
        atributos.add("* Dirección: " + (direccion == null ? "(no se ha fijado)" : direccion));
        atributos.add("* Fecha de nacimiento: " + (fechaNacimiento == null ? "(no se ha fijado)" : fechaNacimiento));
        return atributos.toString();
    }

    public TalkResponse editarNombre(TalkRequest call) {
        return editarAtributo(call, "nombre", ".+");
    }

    public TalkResponse editarChatIdTelegram(TalkRequest call) {
        return editarAtributo(call, "chatIdTelegram", "[0-9]+");
    }

    public TalkResponse editarTelefonoMovil(TalkRequest call) {
        return editarAtributo(call, "telefonoMovil", "\\+?[0-9]+");
    }

    public TalkResponse editarTelefonoFijo(TalkRequest call) {
        return editarAtributo(call, "telefonoFijo", "\\+?[0-9]+");
    }

    public TalkResponse editarDireccion(TalkRequest call) {
        return editarAtributo(call, "direccion", ".+");
    }

    public TalkResponse editarFechaNacimiento(TalkRequest call) {
        return editarAtributo(call, "fechaNacimiento", "[0-9]{2}[-/][0-9]{2}[-/][0-9]{4}");
    }

    public TalkResponse editarAtributo(TalkRequest call, String atributo, String regex) {
        getContext().set(call.getChatId(), "persona_atributo_EN_EDICION", atributo);
        String valor = getContext().getString(call.getChatId(), "persona_atributo_" + atributo);

        return TalkResponse.ofText(
            call.getChatId(),
            "El atributo '" + atributo + "' actual es: " + (valor != null ? valor : "(no se ha fijado el atributo)") +
                "\n/borrar el valor actual del atributo" +
                "\nIntroduce el nuevo valor del atributo:"
            )
            .withRegex("xarxa.XarxaPersonas::editarAtributoFijar", "/borrar|" + regex);
    }

    public TalkResponse editarAtributoFijar(TalkRequest call) {
        String atributo = getContext().getString(call.getChatId(), "persona_atributo_EN_EDICION");
        String valor = call.getMatch().getRegexMatches().get(0).equalsIgnoreCase("/borrar") ?
            null :
            call.getMatch().getRegexMatches().get(0);
        getContext().set(call.getChatId(), "persona_atributo_" + atributo, valor);

        call.getInputAdapter().sendResponse(TalkResponse.ofText(call.getChatId(),
            "Se ha modificado el atributo '" + atributo + "'"
        ));
        return editarPerfil(call);
    }

    public TalkResponse guardarPerfil(TalkRequest call) {
        String hashidPersona = getContext().getString(call.getChatId(), "hashid_persona");
        String hashidMiPersona = helper.getHashidMiPersona();
        // obtener atributos del contexto
        // guardar atributos de la persona

        String nombre = getContext().getString(call.getChatId(),"persona_atributo_nombre");
        String direccion = getContext().getString(call.getChatId(),"persona_atributo_direccion");
        if (nombre == null || direccion == null) {
            return TalkResponse.ofText(
                call.getChatId(),
                "Se necesita fijar al menos el nombre y la dirección para poder guardar el perfil"
                )
                .withPreserveHandlers();
        }

        if (hashidPersona == null) {
            return TalkResponse.ofText(
                call.getChatId(),
                "Se ha creado tu perfil de persona: /persona_" + helper.getHashidMiPersona() +
                    "\n/menu principal"
                )
                .withEmptyHandlers();
        }

        return TalkResponse.ofText(
            call.getChatId(),
            (hashidPersona.equals(hashidMiPersona) ?
                    "Tu perfil ha sido guardado" :
                    "Se ha guardado el perfil de /persona_" + hashidPersona) +
                "\nVolver a perfiles de /personas" +
                "\n/menu principal"
            )
            .withEmptyHandlers();
    }

    // BUSCAR PERSONA

    public TalkResponse buscarPersona(TalkRequest call) {
        helper.resetearCriterio(TipoBusqueda.PERSONA, call.getChatId(), this);
        return criterioBusqueda(call);
    }

    public TalkResponse criterioBusqueda(TalkRequest call) {
        String criterio = helper.construirTextoCriterio(TipoBusqueda.PERSONA, call.getChatId(), this);

        return TalkResponse.ofText(
            call.getChatId(),
            "Fijar criterios:" +
                "\n* /nombre" +
                "\n* Usuario de /telegram" +
                "\n* /direccion" +
                "\n* Si /participa en soportes" +
                "\n\nEl criterio actual de busqueda es:" +
                "\n" + (criterio.isEmpty() ? "NO HAY CRITERIOS FIJADOS" : criterio) +
                "\n\n/limpiar criterios de búsqueda" +
                "\n/buscar según los criterios fijados"
            )
            .withString("xarxa.XarxaPersonas::criterioNombre", "/nombre")
            .withString("xarxa.XarxaPersonas::criterioTelegram", "/telegram")
            .withString("xarxa.XarxaPersonas::criterioDireccion", "/direccion")
            .withString("xarxa.XarxaPersonas::criterioConSoportes", "/participa")
            .withString("xarxa.XarxaPersonas::limpiarCriterio", "/limpiar")
            .withString("xarxa.XarxaPersonas::efectuarBusqueda", "/buscar");
    }

    public TalkResponse limpiarCriterio(TalkRequest call) {
        call.getInputAdapter().sendResponse(TalkResponse.ofText(call.getChatId(),
            "Se han limpiado los criterios de búsqueda"
        ));
        return buscarPersona(call);
    }

    public TalkResponse criterioNombre(TalkRequest call) {
        return TalkResponse.ofText(
            call.getChatId(),
            "Introduce el nombre de la persona o una parte del nombre:" +
                "\nIr a /criterios"
        )
        .withRegex("xarxa.XarxaPersonas::criterioNombreFijar", ".*");
    }

    public TalkResponse criterioNombreFijar(TalkRequest call) {
        if (call.getMatch().getRegexMatches().get(0).equalsIgnoreCase("/criterios")) {
            return criterioBusqueda(call);
        }

        getContext().set(call.getChatId(), "persona_nombre", call.getMatch().getRegexMatches().get(0));
        call.getInputAdapter().sendResponse(TalkResponse.ofText(call.getChatId(),
            "Se ha asignado el criterio de búsqueda por nombre de persona"
        ));
        return criterioBusqueda(call);
    }

    public TalkResponse criterioTelegram(TalkRequest call) {
        return TalkResponse.ofText(
            call.getChatId(),
            "Introduce el usuario de Telegram de la persona:" +
                "\nIr a /criterios"
        )
        .withRegex("xarxa.XarxaPersonas::criterioTelegramFijar", ".*");
    }

    public TalkResponse criterioTelegramFijar(TalkRequest call) {
        if (call.getMatch().getRegexMatches().get(0).equalsIgnoreCase("/criterios")) {
            return criterioBusqueda(call);
        }

        call.getInputAdapter().sendResponse(TalkResponse.ofText(call.getChatId(),
            "Se ha asignado el criterio de búsqueda por usuario de Telegram"
        ));
        getContext().set(call.getChatId(), "persona_telegram", call.getMatch().getRegexMatches().get(0));
        return criterioBusqueda(call);
    }

    public TalkResponse criterioConSoportes(TalkRequest call) {
        return TalkResponse.ofText(
            call.getChatId(),
            "Indica si quieres filtrar según si las personas participan en soportes o no:" +
                "\n/si" +
                "\n/no" +
                "\n/indiferente" +
                "\n\nIr a /criterios"
        )
        .withRegex("xarxa.XarxaPersonas::criterioConSoportesFijar", "/(.*)");
    }

    public TalkResponse criterioDireccion(TalkRequest call) {
        return TalkResponse.ofText(
            call.getChatId(),
            "Introduce la dirección de la persona o una parte de ella:" +
                "\nIr a /criterios"
        )
        .withRegex("xarxa.XarxaPersonas::criterioDireccionFijar", ".*");
    }

    public TalkResponse criterioDireccionFijar(TalkRequest call) {
        if (call.getMatch().getRegexMatches().get(0).equalsIgnoreCase("/criterios")) {
            return criterioBusqueda(call);
        }

        getContext().set(call.getChatId(), "persona_direccion", call.getMatch().getRegexMatches().get(0));
        call.getInputAdapter().sendResponse(TalkResponse.ofText(call.getChatId(),
            "Se ha asignado el criterio de búsqueda por dirección"
        ));
        return criterioBusqueda(call);
    }

    public TalkResponse criterioConSoportesFijar(TalkRequest call) {
        if (call.getMatch().getRegexMatches().get(0).equalsIgnoreCase("/criterios")) {
            return criterioBusqueda(call);
        }

        String conSoportes = call.getMatch().getRegexMatches().get(1);
        if (conSoportes.equalsIgnoreCase("indiferente")) {
            conSoportes = null;
        }
        getContext().set(call.getChatId(), "persona_conSoportes", conSoportes);

        call.getInputAdapter().sendResponse(TalkResponse.ofText(call.getChatId(),
            "Se ha asignado el criterio de búsqueda por si hay participación o no en soportes"
        ));
        return criterioBusqueda(call);
    }

    public TalkResponse efectuarBusqueda(TalkRequest call) {
        Map<String, String> personas = helper.getPersonasBuscadas();
        String textoPersonas = personas.entrySet().stream().map(
            entry -> "* /persona_" + entry.getKey() + " " + entry.getValue()
        ).collect(Collectors.joining("\n"));

        return TalkResponse.ofText(
            call.getChatId(),
            "Estos son las personas encontradas según el criterio fijado:" +
                "\n\n" + textoPersonas +
                "\n\n(Selecciona una persona para tener sus datos en memoria)" +
                "\nContinuar la /busqueda" +
                "\n/menu principal"
        )
        .withString("xarxa.XarxaPersonas::criterioBusqueda", "/busqueda");
    }

    // FICHA PERSONA

    public TalkResponse persona(TalkRequest call) {
        String hashidPersona = call.getMatch().getRegexMatches().get(1);
        String fichaPersona = helper.getFichaPersona(hashidPersona);

        if (fichaPersona != null) {
            getContext().set(call.getChatId(), "hashid_persona", hashidPersona);
            call.getInputAdapter().sendResponse(TalkResponse.ofText(call.getChatId(),
                fichaPersona
            ));
        }
        return TalkResponse.ofText(
            call.getChatId(),
            (fichaPersona == null ?
                    "NO SE HA ENCONTRADO UNA PERSONA CON EL CODIGO INDICADO" :
                    "(Se ha guardado los datos de la persona en la memoria)") +
                "\nPerfiles de /personas" +
                "\n/menu principal"
            )
            .withPreserveHandlers();
    }
}
