package org.app;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.flywaydb.core.Flyway;
import org.logicas.librerias.talks.api.ConfigInterceptorApi;
import org.logicas.librerias.talks.config.Constants;

import javax.management.AttributeNotFoundException;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;
import java.util.function.Supplier;
import java.util.regex.Pattern;

public class AppConfig {

    private static Pattern regex = Pattern.compile("(?<!\\$)\\$\\{([A-Za-z0-9_]+)\\}");
    private static Logger logger = LogManager.getLogger(AppConfig.class);
    private static AppConfig instance = getInstance();

    private Properties properties;
    private ConfigInterceptorApi interceptor;

    private AppConfig() throws IOException, AttributeNotFoundException {
        readProperties();
        checkProperties();
    }

    public static synchronized AppConfig getInstance() {
        if (instance == null) {
            try {
                instance = new AppConfig();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
        return instance;
    }

    public void init() {
        migrateDatabase();
    }

    private static void migrateDatabase() {
        logger.info("Migrating database....");
        Flyway flyway = Flyway.configure().dataSource(
            AppSetting.get(AppSetting.FLYWAY_URL),
            AppSetting.get(AppSetting.FLYWAY_USER),
            AppSetting.get(AppSetting.FLYWAY_PASSWORD)
        ).load();
        flyway.migrate();
        logger.info("Database migrated (if you get an SSLException please ignore it).");
    }

    private void readProperties() throws IOException {
        FileInputStream fis = new FileInputStream(Constants.PROPERTIES_FILE);
        Properties tmpProperties = new Properties();
        tmpProperties.load(fis);
        properties = interpolateEnvironmentVariables(tmpProperties);
    }

    private Properties interpolateEnvironmentVariables(Properties inProperties) {
        Properties tmpProperties = new Properties();
        for (Map.Entry<Object, Object> property : inProperties.entrySet()) {
            String propertyKey = property.getKey().toString();
            String inPropertyValue = property.getValue().toString();
            String outPropertyValue = interpolateEnvironmentVariables(inPropertyValue);
            tmpProperties.setProperty(propertyKey, outPropertyValue);
        }
        return tmpProperties;
    }

    private String interpolateEnvironmentVariables(String inPropertyValue) {
        String outPropertyValue = inPropertyValue;
        for (Map.Entry<String, String> envVar : System.getenv().entrySet()) {
            String envVarName = envVar.getKey();
            String envVarValue = envVar.getValue();
            outPropertyValue = interpolateEnvironmentVariable(outPropertyValue, envVarName, envVarValue);
        }
        return outPropertyValue;
    }

    private String interpolateEnvironmentVariable(String inPropertyValue, String envVarName, String envVarValue) {
        String outPropertyValue = regex.matcher(inPropertyValue).replaceAll(mr -> {
            String possibleEnvVarName = mr.group(1);
            return (possibleEnvVarName != null && possibleEnvVarName.equals(envVarName)) ? envVarValue : "\\${" + possibleEnvVarName + "}";
        });
        return outPropertyValue;
    }

    private void checkProperties() throws AttributeNotFoundException {
        logger.info("Checking all properties are set....");
        for (AppSetting setting : AppSetting.values()) {
            String value = properties.getProperty(setting.name());
            if (value == null) {
                logger.error(String.format("Property [%s] is not set!!", setting.name()));
                throw new AttributeNotFoundException(setting.name());
            }
        }
    }

    public void setInterceptor(ConfigInterceptorApi configInterceptor) {
        this.interceptor = configInterceptor;
    }

    public String getString(AppSetting setting) {
        String settingName = setting.name();
        Supplier<String> defaultSupplier = () -> properties.getProperty(settingName);

        return interceptor == null ?
            defaultSupplier.get() :
            interceptor.getProperty(settingName, defaultSupplier);
    }
}
