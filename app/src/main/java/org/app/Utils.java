package org.app;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.logicas.librerias.talks.api.TalksConfiguration;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.Normalizer;
import java.util.concurrent.TimeUnit;

public class Utils {

    private static final int DELETE_TMP_FILE_AFTER_SECONDS = 30;

    private static Logger logger = LogManager.getLogger(Utils.class);

    public static File fileFrom(TalksConfiguration talksConfig, String filename) {
        if (filename == null) {
            return null;
        }
        ClassLoader classLoader = ClassLoader.getSystemClassLoader();
        URL url = classLoader.getResource(filename);
        if (url == null) {
            logger.warn("Can not load resource " + filename);
            return null;
        }
        //return new File(url.getFile());

        // temporary solution to read files contained in JAR files
        try (InputStream stream = classLoader.getResourceAsStream(filename)) {
            File tmpFile = File.createTempFile("resource_file_", ".file");
            FileUtils.copyInputStreamToFile(stream, tmpFile);
            talksConfig.getFileSystem().deleteAfter(tmpFile, DELETE_TMP_FILE_AFTER_SECONDS, TimeUnit.SECONDS);
            return tmpFile;
        } catch (IOException e) {
            logger.warn("Can not load resource " + filename + " into InputStream");
            return null;
        }
    }

    public static String normalizeInternationalCharacters(String text) {
        if (text == null) {
            return null;
        }

        text = Normalizer.normalize(text, Normalizer.Form.NFD);
        text = text.replaceAll("\\p{M}", "");
        return text;
    }
}
