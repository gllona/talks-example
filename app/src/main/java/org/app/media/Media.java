package org.app.media;

import com.pengrad.telegrambot.model.Location;
import com.pengrad.telegrambot.model.PhotoSize;
import com.pengrad.telegrambot.model.Video;
import java.io.File;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import org.logicas.librerias.talks.api.FileSystemApi;
import org.logicas.librerias.talks.api.TalkType;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.engine.InputHandler;
import org.logicas.librerias.talks.engine.InputTrigger;
import org.logicas.librerias.talks.engine.MediaType;
import org.logicas.librerias.talks.engine.Talk;
import org.logicas.librerias.talks.engine.TalkRequest;
import org.logicas.librerias.talks.engine.TalkResponse;

public class Media extends Talk {

    FileSystemApi fileSystem = talksConfig.getFileSystem();

    public Media(TalksConfiguration talksConfig, TalkType talkType) {
        super(talksConfig, talkType);
        createContext();
    }

    @Override
    protected Optional<InputHandler> getDefaultHandler() {
        return Optional.of(InputHandler.of("media.Media::menu", InputTrigger.ofString("/media")));
    }

    public TalkResponse menu(TalkRequest call) {
        return TalkResponse.ofText(
            call.getChatId(),
            "What to upload:" +
                "\n/location" +
                "\n/photo" +
                "\n/wrongPhoto" +
                "\n/video" +
                "\n/lastVideo"
        )
       .withString("media.Media::inputLocation", "/location")
       .withString("media.Media::inputPhoto", "/photo")
       .withString("media.Media::sendWrongPhoto", "/wrongPhoto")
       .withString("media.Media::inputVideo", "/video")
       .withString("media.Media::lastVideo", "/lastVideo");
    }

    public TalkResponse inputLocation(TalkRequest call) {
        return TalkResponse.ofText(
            call.getChatId(),
            "Please attach a location now:"
        )
       .withMedia("media.Media::uploadLocation", MediaType.LOCATION);
    }

    public TalkResponse uploadLocation(TalkRequest call) {
        Location location = (Location)call.getMatch().getMediaMatch();
        call.getInputAdapter().sendResponse(TalkResponse.ofText(
            call.getChatId(),
            "The location you sent is: LAT=" + location.latitude() + " LON=" + location.longitude() +
                "\nThis is the location back:"
            )
        );
        call.getInputAdapter().sendResponse(TalkResponse.ofLocation(
            call.getChatId(),
            location.latitude(),
            location.longitude()
            )
        );
        return TalkResponse.ofText(
            call.getChatId(),
            "/media"
        );
    }

    public TalkResponse inputPhoto(TalkRequest call) {
        return TalkResponse.ofText(
            call.getChatId(),
            "Please attach a photo now:"
        )
        .withMedia("media.Media::uploadPhoto", MediaType.PHOTO);
    }

    public TalkResponse uploadPhoto(TalkRequest call) {
        PhotoSize[] photos = (PhotoSize[]) call.getMatch().getMediaMatch();
        //String fileId = photos[0].fileId();   // most lo-res photo
        String fileId = photos[photos.length - 1].fileId();   // most hi-res photo
        File file = call.getInputAdapter().getFile(fileId);
        fileSystem.deleteAfter(file, 30, TimeUnit.SECONDS);

        call.getInputAdapter().sendResponse(TalkResponse.ofText(
            call.getChatId(),
            "The photo you sent is: FILENAME=" + file.getName() +
                "\nAnd this is your photo back:"
            )
        );
        call.getInputAdapter().sendResponse(TalkResponse.ofPhoto(
            call.getChatId(),
            file
        ));
        return TalkResponse.ofText(
            call.getChatId(),
            "/media"
        );
    }

    public TalkResponse sendWrongPhoto(TalkRequest call) {
        File wrongFile = new File("/");

        call.getInputAdapter().sendResponse(TalkResponse.ofText(
            call.getChatId(),
            "A wrong photo will be sent:"
            )
        );
        call.getInputAdapter().sendResponse(TalkResponse.ofPhoto(
            call.getChatId(),
            wrongFile
        ));
        return TalkResponse.ofText(
            call.getChatId(),
            "/media"
        );
    }

    public TalkResponse inputVideo(TalkRequest call) {
        return TalkResponse.ofText(
            call.getChatId(),
            "Please attach a video now:"
        )
        .withMedia("media.Media::uploadVideo", MediaType.VIDEO);
    }

    public TalkResponse uploadVideo(TalkRequest call) {
        Video video = (Video) call.getMatch().getMediaMatch();
        String fileId = video.fileId();
        File file = call.getInputAdapter().getFile(fileId);
        fileSystem.deleteAfter(file, 1, TimeUnit.HOURS);

        getContext().setGlobal("LAST_VIDEO_PATH", file.getAbsolutePath());

        call.getInputAdapter().sendResponse(TalkResponse.ofText(
            call.getChatId(),
            "The video you sent is: FILENAME=" + file.getName() +
                "\nAnd this is your video back:"
            )
        );
        call.getInputAdapter().sendResponse(TalkResponse.ofVideo(
            call.getChatId(),
            file
        ));
        return TalkResponse.ofText(
            call.getChatId(),
            "/media"
        );
    }

    public TalkResponse lastVideo(TalkRequest call) {
        String lastVideoPath = (String) getContext().getGlobal("LAST_VIDEO_PATH");
        File lastVideo = lastVideoPath == null ? null : new File(lastVideoPath);

        if (lastVideoPath == null || ! lastVideo.exists()) {
            return TalkResponse.ofText(
                call.getChatId(),
                "There are not uploaded videos recently" +
                    "\n/media"
            );
        }

        call.getInputAdapter().sendResponse(TalkResponse.ofText(
            call.getChatId(),
            "Last uploaded video:"
        ));
        call.getInputAdapter().sendResponse(TalkResponse.ofVideo(
            call.getChatId(),
            lastVideo
        ));
        return TalkResponse.ofText(
            call.getChatId(),
            "/media"
        );
    }
}
