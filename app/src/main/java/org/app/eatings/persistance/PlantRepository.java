package org.app.eatings.persistance;

import org.logicas.librerias.copersistance.Dao;
import org.logicas.librerias.copersistance.repositories.QueryBuilder;
import org.logicas.librerias.copersistance.repositories.SingleKeyEntityRepository;

public class PlantRepository extends SingleKeyEntityRepository<Plant> {

    private static PlantRepository instance;

    private Dao dao;

    private PlantRepository(Dao dao) {
        this.dao = dao;
    }

    public static synchronized PlantRepository getInstance(Dao dao) {
        if (instance == null) {
            instance = new PlantRepository(dao);
        }
        return instance;
    }

    private static final String TABLE = "test_plant";

    @Override
    protected Dao dao() {
        return dao;
    }

    @Override
    public String tableName() {
        return TABLE;
    }

    @Override
    protected String[] columnNames() {
        return Plant.FIELDS;
    }

    @Override
    public String idFieldName() {
        return Plant.FIELD_ID;
    }

    public Plant findOneByName(String name) {
        return this.readOne("name = '" + QueryBuilder.escapeSql(name) + "'");
    }

    public void deleteByName(String name) {
        this.deleteByCondition("name = '" + QueryBuilder.escapeSql(name) + "'");
    }
}
