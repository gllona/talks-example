package org.app.eatings.persistance;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.logicas.librerias.copersistance.entities.DualKeyEntity;

@AllArgsConstructor
@Setter
@Getter
@ToString
public class Eating extends DualKeyEntity {

    public static final String FIELD_EATER_ID       = "eater_id";
    public static final String FIELD_FOOD_ID        = "food_id";
    public static final String FIELD_LAST_EATING_AT = "last_eating_at";

    public static final String[] FIELDS = {
        FIELD_EATER_ID,
        FIELD_FOOD_ID,
        FIELD_LAST_EATING_AT
    };

    private static final String[] KEYS = {
        FIELD_EATER_ID,
        FIELD_FOOD_ID
    };

    private Long eaterId;
    private Long foodId;
    private final Timestamp lastEatingAt;

    @Override
    public Long getId1() {
        return eaterId;
    }

    @Override
    public Long getId2() {
        return foodId;
    }

    @Override
    public void setIds(Long id1, Long id2) {
        this.eaterId = id1;
        this.foodId = id2;
    }

    @Override public Map<String, Object> parameters() {
        Map<String, Object> params = new HashMap<>();

        conditionalAddToParams(eaterId, FIELD_EATER_ID, params);
        conditionalAddToParams(foodId, FIELD_FOOD_ID, params);
        conditionalAddToParams(lastEatingAt, FIELD_LAST_EATING_AT, params);

        return params;
    }

    @Override
    public String[] getKeyColumns() {
        return KEYS;
    }
}
