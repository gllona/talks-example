package org.app.eatings;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import org.app.eatings.persistance.Animal;
import org.app.eatings.persistance.AnimalRepository;
import org.app.eatings.persistance.Eating;
import org.app.eatings.persistance.EatingRepository;
import org.app.eatings.persistance.Plant;
import org.app.eatings.persistance.PlantRepository;
import org.logicas.librerias.copersistance.Dao.DaoConnection;
import org.logicas.librerias.talks.api.TalkType;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.engine.InputHandler;
import org.logicas.librerias.talks.engine.InputTrigger;
import org.logicas.librerias.talks.engine.Talk;
import org.logicas.librerias.talks.engine.TalkRequest;
import org.logicas.librerias.talks.engine.TalkResponse;

public class Eatings extends Talk {

    AnimalRepository animalRepository;
    PlantRepository plantRepository;
    EatingRepository eatingRepository;

    public Eatings(TalksConfiguration talksConfig, TalkType talkType) {
        super(talksConfig, talkType);
        createContext();
        plantRepository = PlantRepository.getInstance(talksConfig.getDao());
        animalRepository = AnimalRepository.getInstance(talksConfig.getDao());
        eatingRepository = EatingRepository.getInstance(talksConfig.getDao());
    }

    @Override
    protected Optional<InputHandler> getDefaultHandler() {
        return Optional.of(InputHandler.of("eatings.Eatings::eatings", InputTrigger.ofString("/eatings")));
    }

    // MAIN MENU

    public TalkResponse eatings(TalkRequest call) {
        return TalkResponse.ofText(
            call.getChatId(),
            "Animals eat Plants" +
                "\n/history" +
                "\n/plants" +
                "\n/animals" +
                "\n/dinners"
        )
        .withString("eatings.EatingsToo::history", "/history")
        .withString("eatings.Eatings::plants", "/plants")
        .withString("eatings.Eatings::animals", "/animals")
        .withString("eatings.Eatings::dinners", "/dinners");
    }

    // PLANTS

    public TalkResponse plants(TalkRequest call) {
        return TalkResponse.ofText(
            call.getChatId(),
            "Plants are older than us" +
                "\n/list" +
                "\n/add" +
                "\n/remove" +
                "\n/edit" +
                "\n/eatings"
        )
        .withString("eatings.Eatings::listPlants", "/list")
        .withString("eatings.Eatings::addPlantInputName", "/add")
        .withString("eatings.Eatings::removePlantChoose", "/remove")
        .withString("eatings.Eatings::editPlantChoose", "/edit");
    }

    public TalkResponse listPlants(TalkRequest call) {
        List<Plant> plants = plantRepository.readAll();
        String plantsList = plants.stream().map(
            plant -> plant.getName() + " is " + plant.getAge() + " years old"
        ).collect(Collectors.joining("\n"));

        return TalkResponse.ofText(
            call.getChatId(),
            "The Plants" +
                "\n" + plantsList +
                "\n/plants"
        )
        .withString("eatings.Eatings::plants", "/plants");
    }

    public TalkResponse addPlantInputName(TalkRequest call) {
        return TalkResponse.ofText(
            call.getChatId(),
            "Enter the plant name to add:"
        )
        .withRegex("eatings.Eatings::addPlantInputAge", "[a-zA-Z0-9 ]+");
    }

    public TalkResponse addPlantInputAge(TalkRequest call) {
        getContext().set(call.getChatId(), "plant_name", call.getMatch().getRegexMatches().get(0));

        return TalkResponse.ofText(
            call.getChatId(),
            "Enter the biological age of the plant (in years):"
        )
        .withRegex("eatings.Eatings::addPlantDo", "[0-9]+");
    }

    public TalkResponse addPlantDo(TalkRequest call) {
        plantRepository.save(new Plant(
            null,
            Integer.parseInt(call.getMatch().getRegexMatches().get(0)),
            getContext().getString(call.getChatId(), "plant_name")
        ));

        return TalkResponse.ofText(
            call.getChatId(),
            "Plant recorded" +
                "\n/list plants" +
                "\n/plants menu"
        )
        .withString("eatings.Eatings::listPlants", "/list")
        .withString("eatings.Eatings::plants", "/plants");
    }

    public TalkResponse removePlantChoose(TalkRequest call) {
        List<Plant> plants = plantRepository.readAll();
        String plantsList = plants.stream().map(
            plant -> "/" + plant.getName().replace(" ", "_")
        ).collect(Collectors.joining("\n"));

        return TalkResponse.ofText(
            call.getChatId(),
            "Choose the plant to remove:" +
                "\n" + plantsList
        )
        .withRegex("eatings.Eatings::removePlantDo", "/(.+)");
    }

    public TalkResponse removePlantDo(TalkRequest call) {
        Plant plant = plantRepository.findOneByName(call.getMatch().getRegexMatches().get(1).replace("_", " "));
        plantRepository.delete(plant);
        // alternative:
        //plantRepository.deleteByName(matches.get(1));

        return TalkResponse.ofText(
            call.getChatId(),
            "Plant deleted" +
                "\n/plants" +
                "\n/eatings"
        )
        .withString("eatings.Eatings::plants", "/plants");
    }

    public TalkResponse editPlantChoose(TalkRequest call) {
        List<Plant> plants = plantRepository.readAll();
        String plantsList = plants.stream().map(
            plant -> "/" + plant.getName().replace(" ", "_")
        ).collect(Collectors.joining("\n"));

        return TalkResponse.ofText(
            call.getChatId(),
            "Choose the plant to edit:" +
                "\n" + plantsList
        )
        .withRegex("eatings.Eatings::editPlantInputAge", "/(.+)");
    }

    public TalkResponse editPlantInputAge(TalkRequest call) {
        Plant plant = plantRepository.findOneByName(call.getMatch().getRegexMatches().get(1).replace("_", " "));
        getContext().set(call.getChatId(), "plant_id", plant.getId());

        return TalkResponse.ofText(
            call.getChatId(),
            plant.getName() + " is " + plant.getAge() + " years old" +
                "\nEnter new age:"
        )
        .withRegex("eatings.Eatings::editPlantDo", "([0-9]+)");
    }

    public TalkResponse editPlantDo(TalkRequest call) {
        long plantId = getContext().getLong(call.getChatId(), "plant_id");
        Plant plant = plantRepository.readById(plantId);
        plant.setAge(Integer.parseInt(call.getMatch().getRegexMatches().get(0)));
        plantRepository.save(plant);

        return TalkResponse.ofText(
            call.getChatId(),
            "Plant updated" +
                "\n/list plants" +
                "\n/plants menu"
        )
        .withString("eatings.Eatings::listPlants", "/list")
        .withString("eatings.Eatings::plants", "/plants");
    }

    // ANIMALS

    public TalkResponse animals(TalkRequest call) {
        return TalkResponse.ofText(
            call.getChatId(),
            "Animals are smarter than us" +
                "\n/list" +
                "\n/count" +
                "\n/add" +
                "\n/remove" +
                "\n/edit" +
                "\n/eatings"
        )
        .withString("eatings.Eatings::listAnimals", "/list")
        .withString("eatings.Eatings::countAnimals", "/count")
        .withString("eatings.Eatings::addAnimalInputName", "/add")
        .withString("eatings.Eatings::removeAnimalChoose", "/remove")
        .withString("eatings.Eatings::editAnimalChoose", "/edit");
    }

    public TalkResponse listAnimals(TalkRequest call) {
        List<Animal> animals = animalRepository.readAll();
        String animalsList = animals.stream().map(
            animal -> animal.getName() + " has " + animal.getAge() + " years of life"
        ).collect(Collectors.joining("\n"));

        return TalkResponse.ofText(
            call.getChatId(),
            "The Animals" +
                "\n" + animalsList +
                "\n/animals"
        )
        .withString("eatings.Eatings::animals", "/animals");
    }

    public TalkResponse countAnimals(TalkRequest call) {
        try (DaoConnection conn = animalRepository.transactionalConn()) {
            long animalCount = animalRepository.countAll(conn, "TRUE");

            //conn.rollback();   // default at try-with-resources close
            //conn.commit();     // should be explicit

            return TalkResponse.ofText(
                call.getChatId(),
                "The Animals count: " + animalCount +
                    "\n/animals"
            )
            .withString("eatings.Eatings::animals", "/animals");
        }
    }

    public TalkResponse addAnimalInputName(TalkRequest call) {
        return TalkResponse.ofText(
            call.getChatId(),
            "Enter the animal name to add:"
        )
        .withRegex("eatings.Eatings::addAnimalInputAge", "[a-zA-Z0-9 ]+");
    }

    public TalkResponse addAnimalInputAge(TalkRequest call) {
        getContext().set(call.getChatId(), "animal_name", call.getMatch().getRegexMatches().get(0));

        return TalkResponse.ofText(
            call.getChatId(),
            "Enter the biological age of the animal (in years):"
        )
        .withRegex("eatings.Eatings::addAnimalDo", "[0-9]+");
    }

    public TalkResponse addAnimalDo(TalkRequest call) {
        animalRepository.save(new Animal(
            null,
            Integer.parseInt(call.getMatch().getRegexMatches().get(0)),
            getContext().getString(call.getChatId(), "animal_name")
        ));

        return TalkResponse.ofText(
            call.getChatId(),
            "Animal saved" +
                "\n/list animals" +
                "\n/animals menu"
        )
        .withString("eatings.Eatings::listAnimals", "/list")
        .withString("eatings.Eatings::animals", "/animals");
    }

    public TalkResponse removeAnimalChoose(TalkRequest call) {
        List<Animal> animals = animalRepository.readAll();
        String animalsList = animals.stream().map(
            animal -> "/" + animal.getName().replace(" ", "_")
        ).collect(Collectors.joining("\n"));

        return TalkResponse.ofText(
            call.getChatId(),
            "Choose the animal to remove:" +
                "\n" + animalsList
        )
        .withRegex("eatings.Eatings::removeAnimalDo", "/(.+)");
    }

    public TalkResponse removeAnimalDo(TalkRequest call) {
        animalRepository.deleteByName(call.getMatch().getRegexMatches().get(1).replace("_", " "));
        // alternative:
        //Animal animal = animalRepository.findOneByName(matches.get(1));
        //animalRepository.delete(animal);

        return TalkResponse.ofText(
            call.getChatId(),
            "Animal removed" +
                "\n/animals" +
                "\n/eatings"
        )
        .withString("eatings.Eatings::animals", "/animals");
    }

    public TalkResponse editAnimalChoose(TalkRequest call) {
        List<Animal> animals = animalRepository.readAll();
        String animalsList = animals.stream().map(
            animal -> "/" + animal.getName().replace(" ", "_")
        ).collect(Collectors.joining("\n"));

        return TalkResponse.ofText(
            call.getChatId(),
            "Choose the animal to edit:" +
                "\n" + animalsList
        )
        .withRegex("eatings.Eatings::editAnimalInputAge", "/(.+)");
    }

    public TalkResponse editAnimalInputAge(TalkRequest call) {
        Animal animal = animalRepository.findOneByName(call.getMatch().getRegexMatches().get(1).replace("_", " "));
        getContext().set(call.getChatId(), "animal_id", animal.getId());

        return TalkResponse.ofText(
            call.getChatId(),
            animal.getName() + " is " + animal.getAge() + " years old" +
                "\nEnter new age:"
        )
        .withRegex("eatings.Eatings::editAnimalDo", "([0-9]+)");
    }

    public TalkResponse editAnimalDo(TalkRequest call) {
        long animalId = getContext().getLong(call.getChatId(), "animal_id");
        Animal animal = animalRepository.readById(animalId);
        animal.setAge(Integer.parseInt(call.getMatch().getRegexMatches().get(0)));
        animalRepository.save(animal);

        return TalkResponse.ofText(
            call.getChatId(),
            "Animal updated" +
                "\n/list animals" +
                "\n/animals menu"
        )
        .withString("eatings.Eatings::listAnimals", "/list")
        .withString("eatings.Eatings::animals", "/animals");
    }

    // DINNERS

    public TalkResponse dinners(TalkRequest call) {
        return TalkResponse.ofText(
            call.getChatId(),
            "/make a dinner" +
                "\n/list dinners" +
                "\n/eatings"
        )
        .withString("eatings.Eatings::listDinners", "/list")
        .withString("eatings.Eatings::makeDinnerInputEater", "/make");
    }

    public TalkResponse listDinners(TalkRequest call) {
        List<Map<String, Object>> eatings = eatingRepository.findAllWithFullNames();
        String eatingsList = eatings.stream().map(
            entry -> entry.get("animal_name") + " ate " + entry.get("plant_name") + " on " + entry.get("last_eating_at")
        ).collect(Collectors.joining("\n"));

        return TalkResponse.ofText(
            call.getChatId(),
            "The dinners" +
                "\n" + eatingsList +
                "\n/dinners"
        )
        .withString("eatings.Eatings::dinners", "/dinners");
    }

    public TalkResponse makeDinnerInputEater(TalkRequest call) {
        List<Animal> animals = animalRepository.readAll();
        String animalsList = animals.stream().map(
            animal -> "/" + animal.getName().replace(" ", "_")
        ).collect(Collectors.joining("\n"));

        return TalkResponse.ofText(
            call.getChatId(),
            "Who wants to eat?" +
                "\n" + animalsList +
                "\nback to /dinners"
        )
        .withString("eatings.Eatings::dinners", "/dinners")
        .withRegex("eatings.Eatings::makeDinnerInputFood", "/(.+)");
    }

    public TalkResponse makeDinnerInputFood(TalkRequest call) {
        Animal animal = animalRepository.findOneByName(call.getMatch().getRegexMatches().get(1).replace("_", " "));
        getContext().set(call.getChatId(), "animal_id", animal.getId());

        List<Plant> plants = plantRepository.readAll();
        String plantsList = plants.stream().map(
            plant -> "/" + plant.getName().replace(" ", "_")
        ).collect(Collectors.joining("\n"));

        return TalkResponse.ofText(
            call.getChatId(),
            "Who will get eaten?" +
                "\n" + plantsList +
                "\nback to /dinners"
        )
        .withRegex("eatings.Eatings::makeDinnerDo", "/(.+)");
    }

    public TalkResponse makeDinnerDo(TalkRequest call) {
        long animalId = getContext().getLong(call.getChatId(), "animal_id");
        Plant plant = plantRepository.findOneByName(call.getMatch().getRegexMatches().get(1).replace("_", " "));
        long plantId = plant.getId();

        eatingRepository.save(new Eating(
            animalId,
            plantId,
            new Timestamp(Instant.now().toEpochMilli())
        ));

        return TalkResponse.ofText(
            call.getChatId(),
            "Dinner done yum yum!" +
                "\n/list dinners" +
                "\nback to /dinners" +
                "\n/eatings"
        )
        .withString("eatings.Eatings::listDinners", "/list")
        .withString("eatings.Eatings::dinners", "/dinners");
    }
}
