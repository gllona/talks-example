ALTER TABLE talk_contexts
    MODIFY COLUMN session_id BIGINT NOT NULL;

ALTER TABLE xarxa_usuario
    MODIFY COLUMN telegram_chat_id BIGINT NULL;
