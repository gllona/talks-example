ALTER TABLE talks_interactions ADD created_at TIMESTAMP NULL AFTER message_id;

ALTER TABLE talks_interactions MODIFY COLUMN fly_at TIMESTAMP NULL;
