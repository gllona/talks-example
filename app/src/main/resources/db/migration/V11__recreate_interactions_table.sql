DROP TABLE talks_interactions;

CREATE TABLE talks_interactions(
    id BIGINT NOT NULL AUTO_INCREMENT,
    type CHAR(8) NOT NULL,
    chat_id BIGINT NOT NULL,
    message_id BIGINT NOT NULL,
    fly_at TIMESTAMP,
    PRIMARY KEY(id),
    INDEX interactions_fly_at_idx (fly_at)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1;
