# Talks examples

[Talks](https://gitlab.com/gllona/talks) is a mini-framework for Telegram bots written in Java and Kotlin.

This repo offers a working example of how to use Talks for writing your own bots.

This repo is updated to use the `0.1.23-kotlin` version of the library. The library JAR is included in the 
`libs` directory. You can generate the JAR from the library `kotlin` branch.

The best form to see what Talks does is by example. Look into the `app`
subproject and see the subclasses of `Talk`.

There are some examples in the `org.app` package:

* `eatings` implements a simple bot that allows you to add and remove plants and
animals, and plan dinners where animals eat plants. The code is backed by a database
with a thin persistence layer called `copersistance`.

* `xarxa` implements the conversational logic for a mutual support network among
people. No database is implemented yet.

* Some other examples as well

## What you need

* Java 17
* A MySQL / MariaDB server

## How to run

Steps common to all methods:

* Clone the repo
* `cd` to directory
* Register a new Telegram bot (see below)
* `cp config.properties.EXAMPLE config.properties`
* Create your `.env` file based on the example (see below)

### With Gradle

* Create a MySql database and optionally a MySql user with proper rights to the database
* `cd app && ln -s ../config.properties config.properties && cd ..`
* `source .env && export $(cut -d= -f1 .env)`
* `./gradlew :app:runApp --no-daemon`
* Or run the `App` class in your favorite IDE

### With JAR

* Create a MySql database and optionally a MySql user with proper rights to the database
* `cd app && ln -s ../config.properties config.properties && cd ..`
* `source .env && export $(cut -d= -f1 .env)`
* `./gradlew clean shadowJar`
* `java --add-opens java.base/java.lang=ALL-UNNAMED -jar app/build/libs/app-all.jar`

### With Docker

Provided Docker configuration uses MariaDB.

Setup:

* `mkdir db`
* `docker compose build`

To start the service:

* `docker compose up -d`

To shutdown the service:

* `docker compose down`

To reset the database:

* `docker volume prune`
* `rm -rf db/*`

## Configure the bot

You will need to register a Telegram bot, which is associated with your Telegram account. For each account you can 
register a maximum of 20 bots.

To register a Telegram bot use [BotFather](https://t.me/BotFather). Set the bot name, description and get the 
`API Token`

Previous to the first run, you have to create a `.env` configuration file. Make a copy of the provided `.env.EXAMPLE` 
and edit it as follows:

* Database settings:
    * Set the database server name in `DB_SERVER`. For Docker use `mariadb`, else use `localhost`
    * Set the database port in `DB_PORT`. For Docker use a non-used port in your local system like `3307`, else use `3306` 
    * Set the database name in `DB_DATABASE`
    * Set the user in `DB_USERNAME`
    * Set the password in `DB_PASSWORD`
    * If you get trouble working with buttons with emoji, try setting `ENCODE_EMOJI=1`
* Telegram bot settings:
    * Set the bot name (finished in `bot`, `Bot`, etc) in `BOT_NAME`
    * Set the bot API Token in `TELEGRAM_API_KEY`
    * Set the maximum number of updates to retrieve from Telegram in each polling cycle in `TELEGRAM_GET_UPDATES_LIMIT`
* Logging settings:
    * Set `SESSIONS_TO_LOG` to `*` to log all interactions. Or set it to a comma-separated list of Telegram chat ID's.
      To disable logging you can set this value to `-1`
      (Note: chat ID's appear `[between_brackets]` in all logged interactions)
* Bot users settings:
    * Set `MAGIC_CODE` to a four digits sequence. The first time you start the bot, enter this code after the
      `Welcome` message, then tap on `/start`. You will be the bot admin. Use the `/admin` privileged command to
      add other admins and operators. Operators will have access to messages sent by users with the `/contact` command.
      Note: after you assign roles to users, they will have to re-start the bot with `/start`
* Other settings:
    * Set `FALLBACK_RESPONSE` to the text to be used as a default response to the user when there are 
      not matching interactions defined in code
    * Set `USE_PERSISTENT_MESSAGE_SENDER=1` and `PERSISTENT_MESSAGE_SENDER_VERSION=3`
    * If you run the bot on shared hardware, you can set `USE_SINGLE_THREAD=1`. This will override every other
      concurrency setting
    
Some static properties can be overridden at runtime by using the `/admin` command, then 
`Settings`. See the `SettingsTalk` class and the `Setting` enum, where `EMERGENCY_MODE` can be
set to make disappear scheduled flying messages in a fast way (more information about flying messages
is available in the framework website).

## Additional information

* Check the [Talks framework](https://gitlab.com/gllona/talks) website and the `kotlin` branch
* See also the admin utilities included in the [Animal Welfare Telegram Bot](https://gitlab.com/gllona/animal-welfare-telegram-bot).

## Author

Developed by [Gorka Llona](http://desarrolladores.logicos.org/gorka).
